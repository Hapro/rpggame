﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{
    public class JsonData
    {
        public string Data { get; }

        public JsonData(string data)
        {
            Data = data;
        }
    }
}
