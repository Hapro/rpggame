﻿using Microsoft.Xna.Framework.Content;
using Utils;

namespace Utils
{
    public class JsonReader : ContentTypeReader<JsonData>
    {
        protected override JsonData Read(ContentReader input, JsonData existingInstance)
        {
            var length = input.ReadInt32();
            var str = "";

            for (var i = 0; i < length; i++)
                str += (char)input.ReadByte();

            return new JsonData(str);
        }
    }
}
