﻿using System;
using System.Drawing;
using Color = System.Drawing.Color;

namespace Utils
{
    public class PerlinNoise
    {
        private readonly int m_Width;
        private readonly int m_Height;
        private readonly double[,] m_Noise;
        private readonly Random m_Random;
        private readonly int m_ZoomFactor;

        public PerlinNoise(int width, int height, int zoomFactor)
        {
            m_Height = height;
            m_Width = width;
            m_Noise = new double[width, height];
            m_Random = new Random();
            m_ZoomFactor = zoomFactor;

            //Initialize noise with zeros
            for (var x = 0; x < width; x++) for (var y = 0; y < height; y++) m_Noise[x, y] = 0.0;
        }

        public double[,] Generate()
        {
            var bitmap = new double[m_Width, m_Height];

            //Fill array with random numbers
            for (var x = 0; x < m_Width; x++)
            {
                for (var y = 0; y < m_Height; y++)
                {
                    m_Noise[x, y] = m_Random.NextDouble();
                    bitmap[x, y] = 0.0f;
                }
            }

            for (var x = 0; x < m_Width; x++)
            {
                for (var y = 0; y < m_Height; y++)
                {
                    var size = (double)m_ZoomFactor;
                    var value = 0.0;

                    value += getValue(x / size, y / size);

                    bitmap[x, y] = value;
                }
            }

            SaveTestImage(bitmap);

            return bitmap;
        }

        public void SaveTestImage(double[,] bitmap)
        {
            var img = new Bitmap(m_Width, m_Height);

            for (var x = 0; x < m_Width; x++)
            {
                for (var y = 0; y < m_Height; y++)
                {
                    var color = (int)(bitmap[x, y] * 255);
                    img.SetPixel(x, y, Color.FromArgb(color, color, color));
                }
            }

            img.Save("perlinNoise.png");
        }

        private double getValue(double x, double y)
        {
            //get fractional part of x and y
            double fractX = (x - (int)x);
            double fractY = (y - (int)y);

            //wrap around
            int x1 = (int)x;
            int y1 = (int)y;

            //neighbor values
            int x2 = x1 + 1;
            int y2 = y1 + 1;

            //Get noise from each corner
            var n1 = m_Noise[x1, y1];
            var n2 = m_Noise[x1, y2];
            var n3 = m_Noise[x2, y1];
            var n4 = m_Noise[x2, y2];

            double i1 = lerp(n1, n3, fractX);
            double i2 = lerp(n2, n4, fractX);
            double value = lerp(i1, i2, fractY);
            return value;
        }


        private double lerp(double x, double y, double a)
        {
            double value = (1 - Math.Cos(a * Math.PI)) * 0.5;
            return x * (1 - value) + y * value;
        }
    }
}
