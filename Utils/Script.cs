﻿using System;
using System.Collections.Generic;

namespace Utils
{
    public class ScriptParameter
    {
        public Type Type { get; }
        public object Value { get; private set; }
        public bool IsSet { get; private set; }

        public ScriptParameter(Type type)
        {
            Type = type;
            IsSet = false;
        }

        public ScriptParameter(Type type, object value)
        {
            Type = type;
            Set(value);
        }

        public void Set(object value)
        {
            Value = value;
            IsSet = true;
        }
    }

    public class ScriptParameters
    {
        protected Dictionary<string, ScriptParameter> m_Params;

        public ScriptParameters()
        {
            m_Params = new Dictionary<string, ScriptParameter>();
        }

        public object GetParameter(string name)
        {
            return m_Params[name].Value;
        }

        public bool CheckAllSet()
        {
            foreach (var p in m_Params)
            {
                if (!p.Value.IsSet) return false;
            }

            return true;
        }

        public void SetParameter(string name, object value)
        {
            if (m_Params[name].Type == value.GetType())
            {
                m_Params[name].Set(value);
            }
            else
            {
                throw new Exception("Supplied value is wrong type!");
            }
        }

        public virtual void RequireParameters(params Tuple<string, Type>[] parameters)
        {
            foreach (var t in parameters)
            {
                m_Params.Add(t.Item1, new ScriptParameter(t.Item2));
            }
        }
    }

    /// <summary>
    /// Scripts have parameters. All parameters must be set before
    /// execution, otherwise the application will crash
    /// </summary>
    public class Script
    {
        protected ScriptParameters m_Params;
        protected Action<ScriptParameters> m_Script;
        public string Name { get; }

        public Script(string name)
        {
            Name = name;
            m_Script = (x) => { };
            m_Params = new ScriptParameters();
        }

        public Script(string name, Action<ScriptParameters> script)
        {
            Name = name;
            m_Script = script;
            m_Params = new ScriptParameters();
        }

        public void Run()
        {
            if (!m_Params.CheckAllSet()) throw new Exception("Parameter not set!");

            m_Script.Invoke(m_Params);
        }

        public void SetParameter(string name, object value)
        {
            m_Params.SetParameter(name, value);
        }

        public virtual void RequireParameters(params Tuple<string, Type>[] parameters)
        {
            m_Params.RequireParameters(parameters);
        }
    }
}
