﻿using Newtonsoft.Json;

namespace Utils
{
    public static class Extensions
    {
        public static bool IsNumeric(this string value)
        {
            int r;
            int.TryParse(value, out r);
            return r != 0;
        }

        public static T Deserialize<T>(this JsonData json)
        {
            return JsonConvert.DeserializeObject<T>(json.Data);
        }
    }
}
