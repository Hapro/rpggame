﻿using Microsoft.Xna.Framework;

namespace Utils
{
    public class Camera
    {
        public Vector2 Location { get; set; }
        public Vector2 Origin { get; set; }
        public float Zoom { get; set; }
        public float Rotation { get; set; }

        public Camera()
        {
            Location = Vector2.Zero;
            Origin = Vector2.Zero;
            Zoom = 1;
            Rotation = 0;
        }

        public Matrix GetTransform()
        {
            return Matrix.CreateTranslation(new Vector3(Location, 0.0f)) *
                   Matrix.CreateRotationZ(Rotation) *
                   Matrix.CreateScale(Zoom, Zoom, 1.0f) *
                   Matrix.CreateTranslation(new Vector3(Origin, 0.0f));
        }
    }
}
