﻿namespace Utils
{
    public class DifferableInt
    {
        public int Modifier { get; set; }

        public int BaseValue { get; }

        public int Value
        {
            get { return BaseValue + Modifier; }
            set { Modifier = value - BaseValue; }
        }

        public DifferableInt(int value)
        {
            BaseValue = value;
            Modifier = 0;
        }

        public static implicit operator DifferableInt(int value)
        {
            return new DifferableInt(value);
        }

        #region Json serializiation information
        public bool ShouldSerializeModifier() { return false; }
        public bool ShouldSerializeValue() { return false; }
        public bool ShouldSerializeBaseValue() { return true; }
        #endregion
    }
}
