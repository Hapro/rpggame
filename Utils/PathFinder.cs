﻿using System;
using System.Collections.Generic;

namespace Utils
{
    /// <summary>
    /// Interface for anything which wants to be used in pathfinding 
    /// </summary>
    public interface IPathFindingTile2D
    {
        int X { get; }

        int Y { get; }

        bool IsAdjacent(IPathFindingTile2D tile);

        float CostFunction(IPathFindingTile2D tile);
    }

    public class PathFinder
    {
        /// <summary>
        /// Get path to using Dijkstra's greedy search
        /// </summary>
        /// <param name="start">Start item</param>
        /// <param name="end">End item</param>
        /// <param name="collection">Collection of items to search</param>
        /// <param name="costFunction">Function to determine the 'cost' to move between two items</param>
        /// <returns>Items in order comprising the shortest path between two points</returns>
        public List<IPathFindingTile2D> GetPathTo(IPathFindingTile2D start, IPathFindingTile2D end,
            IReadOnlyCollection<IPathFindingTile2D> collection)
        {
            var unvisited = new List<Node>();
            var visited = new List<Node>();
            Node currentNode;
            var newCost = 0.0f;

            //Add everything to unvisited
            foreach (var item in collection) unvisited.Add(new Node(item, int.MaxValue, null));

            //Make the current node point to the start tile
            currentNode = unvisited.Find(z => z.Item == start);
            currentNode.Cost = 0;

            while (unvisited.Count > 0)
            {
                //Sort open according to shortest path so far
                SortUnvisited(unvisited);

                //Inspect the cheapest node
                currentNode = unvisited[0];

                foreach (var n in unvisited)
                {
                    //If this node is adjacent and not visited
                    if (currentNode.Item.IsAdjacent(n.Item))
                    {
                        //Calculate the new cost
                        newCost = currentNode.Cost + currentNode.Item.CostFunction(n.Item);

                        //Re assign cost if cheaper
                        if (newCost < n.Cost)
                        {
                            n.Parent = currentNode;
                            n.Cost = newCost;
                        }
                    }
                }

                //Put current node in visited list
                unvisited.Remove(currentNode);
                visited.Add(currentNode);

                //If the end is in the visited list, create the path
                if (visited.Exists(x => x.Item == end))
                {
                    var list = new List<IPathFindingTile2D>();

                    while (currentNode != null)
                    {
                        list.Add(currentNode.Item);
                        currentNode = currentNode.Parent;
                    }

                    list.Reverse();

                    return list;
                }
            }

            return new List<IPathFindingTile2D>();
        }

        private void SortUnvisited(List<Node> lst)
        {
            lst.Sort((a, b) =>
            {
                if (b.Cost > a.Cost)
                    return -1;

                return 1;
            });
        }

        internal class Node
        {
            public Node Parent { get; set; }
            public float Cost { get; set; }
            public IPathFindingTile2D Item { get; set; }

            public Node(IPathFindingTile2D item, int cost, Node parent)
            {
                Parent = parent;
                Item = item;
                Cost = cost;
            }
        }
    }
}
