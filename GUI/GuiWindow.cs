﻿using Graphics;
using GUI.Factories;

namespace GUI
{
    class GuiWindow : GuiComponent
    {
        public GuiWindow(AnimatedSpriteGraphicsComponent graphicsComponent,
            IGuiComponentFactory guiComponentFactory, int width, int height)
            : base(graphicsComponent, guiComponentFactory, width, height)
        {
        }
    }
}
