﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI
{
    /// <summary>
    /// Class used for organising GUI elements
    /// </summary>
    public class GuiGrid
    {
        public Vector2 Location { get; set; }

        /// <summary>
        /// Width in 'Units'
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Height in 'Units'
        /// </summary>
        public int Height { get; set; }

        //Unit defines width + height of grid square
        public int Unit { get; set; }

        private readonly Dictionary<Tuple<int, int>, IGuiComponent> m_ComponentLocations;

        public GuiGrid()
        {
            Location = Vector2.Zero;
            m_ComponentLocations = new Dictionary<Tuple<int, int>, IGuiComponent>();
        }

        public void AddComponent(IGuiComponent component, int x, int y)
        {
            if(!m_ComponentLocations.ContainsKey(Tuple.Create(x, y)))
            {
                m_ComponentLocations.Add(Tuple.Create(x, y), component);
            }
        }

        /// <summary>
        /// Update positions of components relative to grid
        /// </summary>
        public void Refresh()
        {
            foreach(var kvp in m_ComponentLocations)
            {
                var x = kvp.Key.Item1 * Unit * 1.3f;
                var y = kvp.Key.Item2 * Unit * 1.3f;

                kvp.Value.SetLocation(new Vector2(x, y) + Location);
            }
        }
    }
}
