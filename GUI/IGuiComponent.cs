﻿using Microsoft.Xna.Framework;

namespace GUI
{
    public interface IGuiComponent
    {
        bool Focus { get; set; }

        GuiComponentEvents GuiComponentEvents{ get; }

        void Draw();

        void Update(float elapsed);

        void SetLocation(Vector2 location);

        Vector2 GetLocation();

        Rectangle GetClickableBounds();

        void Hide();

        void Show();

        //These methods fire off events

        void TextInput(char letters);

        void PointerHover(Vector2 location);

        void PointerClick(Vector2 location);

        void PointerDown(Vector2 location);
    }
}
