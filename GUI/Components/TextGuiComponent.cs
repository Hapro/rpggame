﻿using GUI.ComponentInterface;
using Graphics;
using GUI.Factories;
using Microsoft.Xna.Framework;

namespace GUI.Components
{
    /// <summary>
    /// Describes any component which will display some text,
    /// which is almost everything
    /// </summary>
    public class TextGuiComponent : GuiComponent
    {
        public string Text
        {
            get { return Label.Text; }
            set { Label.Text = value; }
        }

        protected readonly ILabel Label;

        public TextGuiComponent(AnimatedSpriteGraphicsComponent graphicsComponent,
            IGuiComponentFactory guiComponentFactory, int width, int height)
            : base(graphicsComponent, guiComponentFactory, width, height)
        {
            Label = guiComponentFactory.CreateLabel(
                Location,
                width,
                height,
                Color.Black,
                "");

            SubComponents.Add(Label);
        }
    }

    public class TextInputGuiComponent : TextGuiComponent, ITextInput
    {
        public TextInputGuiComponent(AnimatedSpriteGraphicsComponent graphicsComponent,
            IGuiComponentFactory guiComponentFactory, int width, int height)
            : base(graphicsComponent, guiComponentFactory, width, height)
        {
            Label.SetLocation(GetCenter());
        }

        public override void TextInput(char letters)
        {
            //8 = backspace
            if (letters != '\0')
            {
                Text += letters;
            }
            else
            {
                if (Text.Length > 0) Text = Text.Remove(Text.Length - 1);
            }
        }
    }
}
