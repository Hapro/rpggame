﻿using GUI.ComponentInterface;
using Graphics;
using GUI.Factories;

namespace GUI.Components
{
    /// <summary>
    /// Frame indices:
    /// 0 == normal
    /// 1 == hovering
    /// 2 == mouse down
    /// </summary>
    public class ButtonComponent : TextGuiComponent, IButton
    {
        public bool Pressed { get; set; }

        public ButtonComponent(AnimatedSpriteGraphicsComponent graphicsComponent,
            IGuiComponentFactory guiComponentFactory,
            int width, int height)
            : base(graphicsComponent, guiComponentFactory, width, height)
        {
            Pressed = false;
            Label.SetLocation(GetCenter());
        }
    }
}
