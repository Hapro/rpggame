﻿using GUI.ComponentInterface;
using Graphics;
using GUI.Factories;

namespace GUI.Components
{
    public class NumberGuiComponent : TextGuiComponent, INumberInput
    {
        public int NumberValue
        {
            get { int v = -1;  int.TryParse(Text, out v); return v;  }
            set { Text = value.ToString(); }
        }

        public NumberGuiComponent(AnimatedSpriteGraphicsComponent graphicsComponent,
            IGuiComponentFactory guiComponentFactory, int width, int height)
            : base(graphicsComponent, guiComponentFactory, width, height)
        {
            Label.SetLocation(GetCenter());
        }

        public override void TextInput(char letters)
        {
            //8 = backspace
            if (letters != '\0')
            {
                //Only add value if it is a number
                if(char.IsNumber(letters))Text += letters;
            }
            else
            {
                if (Text.Length > 0) Text = Text.Remove(Text.Length - 1);
            }
        }
    }
}
