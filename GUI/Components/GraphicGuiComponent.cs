﻿using Graphics;
using GUI.ComponentInterface;
using GUI.Factories;

namespace GUI.Components
{
    /// <summary>
    /// GuiComponent wrapper for any kind of graphics
    /// </summary>
    public class GraphicGuiComponent : GuiComponent, IWindow
    {
        public GraphicGuiComponent(AnimatedSpriteGraphicsComponent graphicsComponent, 
            IGuiComponentFactory guiComponentFactory, int width, int height)
            : base(graphicsComponent, guiComponentFactory, width, height)
        {
        }
    }
}
