﻿using System.Collections.Generic;
using Graphics;
using GUI.ComponentInterface;
using Microsoft.Xna.Framework;
using GUI.Factories;

namespace GUI.Components
{
    class ScrollList : GuiComponent, IScrollList
    {
        public List<string> Items { get; }
        public int SelectedIndex { get; private set; }
        public string SelectedItem { get { return Items[SelectedIndex]; } }

        private int m_startDrawIndex;
        private int m_endDrawIndex;
        private const int ITEM_HEIGHT = 25;

        private IButton m_ScrollUpButton;
        private IButton m_ScrollDownButton;
        private List<ILabel> ItemLabels;

        //TODO:
        //Pass GuiComponentFactory to every gui component
        //Use ILabels to draw text on every gui component,
        //  instead of passing a TextGraphicsComponent

        public ScrollList(AnimatedSpriteGraphicsComponent graphicsComponent,
            IGuiComponentFactory guiComponentFactory,
            int width, int height)
            : base(graphicsComponent, guiComponentFactory, width, height)
        {
            m_startDrawIndex = 0;
            SelectedIndex = 0;

            ItemLabels = new List<ILabel>();
            
            m_ScrollDownButton = guiComponentFactory.CreateButton(
                Vector2.Zero, 50, 50, Color.White, "");
            m_ScrollUpButton = guiComponentFactory.CreateButton(
                Vector2.Zero, 50, 50, Color.White, "");

            m_ScrollDownButton.GuiComponentEvents.PointerClicked += ScrollDown;
            m_ScrollUpButton.GuiComponentEvents.PointerClicked += ScrollUp;

            //m_ItemText.Align = Align.Left;
            Items = new List<string>();

            SubComponents.Add(m_ScrollDownButton);
            SubComponents.Add(m_ScrollUpButton);

            UpdateItemsToDraw();
            SetButtonDrawLocations();
        }


        public void ScrollDown()
        {
            if(SelectedIndex > 0) SelectedIndex++;
        }

        public void ScrollUp()
        {
            if(SelectedIndex < Items.Count - 1) SelectedIndex--;
        }

        public override void Draw()
        {
            base.Draw();
        }

        private void SetButtonDrawLocations()
        {
            m_ScrollUpButton.SetLocation(new Vector2(0, 0));
            m_ScrollDownButton.SetLocation(new Vector2(0,
                Height - m_ScrollDownButton.GetClickableBounds().Height));
        }

        public void AddItem(string item)
        {
            var il = CreateItemLabel();
            il.Text = item;

            Items.Add(item);
            ItemLabels.Add(il);
            SubComponents.Add(il);

            il.SetLocation(Location + (new Vector2(0, ITEM_HEIGHT) * Items.Count));

            UpdateItemsToDraw();
        }

        private void UpdateItemsToDraw()
        {
            int noItems = Height / ITEM_HEIGHT;

            m_endDrawIndex = m_startDrawIndex + noItems;

            if (m_endDrawIndex > Items.Count) m_endDrawIndex = Items.Count;
        }

        private ILabel CreateItemLabel()
        {
            return m_GuiComponentFactory.CreateLabel(
                Vector2.Zero,
                Width,
                ITEM_HEIGHT,
                Color.White,
                "");
        }
    }
}
