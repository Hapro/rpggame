﻿using GUI.ComponentInterface;
using Graphics;
using GUI.Factories;
using Microsoft.Xna.Framework;

namespace GUI.Components
{
    public class LabelComponent : GuiComponent, ILabel
    {
        public string Text
        {
            get { return m_TextGraphicsComponent.Text; }
            set { m_TextGraphicsComponent.SetString(value); }
        }

        private readonly TextGraphicsComponent m_TextGraphicsComponent;

        public LabelComponent(TextGraphicsComponent textGraphicsComponent,
            AnimatedSpriteGraphicsComponent backgroundGraphicsComponent,
            IGuiComponentFactory guiComponentFactory, int width, int height)
            : base(backgroundGraphicsComponent.ToAnimated(), guiComponentFactory, width, height)
        {
            m_TextGraphicsComponent = textGraphicsComponent;
            m_TextGraphicsComponent.Align = Align.Center;
            m_TextGraphicsComponent.SetLocation(GetCenter());
        }

        public override void Update(float elapsed)
        {
            m_TextGraphicsComponent.Update(elapsed);
            base.Update(elapsed);
        }

        public override void Draw()
        {
            if(!m_Hidden) m_TextGraphicsComponent.Draw();
        }

        public override void SetLocation(Vector2 location)
        {
            m_TextGraphicsComponent.SetLocation(location);
            base.SetLocation(location);
        }
    }
}
