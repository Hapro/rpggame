﻿using Graphics;
using GUI.ComponentInterface;
using GUI.Factories;

namespace GUI.Components
{
    public class WindowGuiComponent : GuiComponent, IWindow
    {
        public WindowGuiComponent(AnimatedSpriteGraphicsComponent graphicsComponent, 
            IGuiComponentFactory guiComponentFactory, int width, int height)
            : base(graphicsComponent, guiComponentFactory, width, height)
        {
        }
    }
}
