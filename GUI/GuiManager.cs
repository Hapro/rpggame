﻿namespace GUI
{
    public class GuiManager : IGuiManager
    {
        private readonly GuiComponentTree m_GuiComponentTree;
        public UserInput.UserInput UserInput { get; }

        public GuiManager(UserInput.UserInput userInput)
        {
            m_GuiComponentTree = new GuiComponentTree();
            UserInput = userInput;
        }

        public void AddChild(IGuiComponent parent, IGuiComponent child)
        {
            m_GuiComponentTree.AddChild(parent, child);
        }

        public void AddComponent(IGuiComponent component)
        {
            m_GuiComponentTree.AddComponent(component);
        }

        public void Draw()
        {
            foreach(var component in m_GuiComponentTree.GetDrawOrder())
            {
                component.Draw();
            }
        }

        public void Update(float elapsed)
        {
            UserInput.Update();
            UpdateMouse();

            m_GuiComponentTree.Map(x => x.Component.Update(elapsed));

            if (UserInput.KeysPressed.Count > 0) UpdateKeypress();

            UpdateComponentEvents();
        }

        public void UpdateComponentEvents()
        {

        }

        private void UpdateKeypress()
        {
            m_GuiComponentTree.Map(x =>
            {
                foreach (var key in UserInput.KeysStruck)
                {
                    //Only register key pressed on focused components
                    if(x.Component.Focus) x.Component.TextInput(key.ToChar());
                }
            });
        }

        private void UpdateMouse()
        {
            var mouseLocation = UserInput.MouseLocation;

            m_GuiComponentTree.Map((x =>
            {
                if (x.Component.GetClickableBounds().Contains(mouseLocation))
                {
                    x.Component.PointerHover(mouseLocation);

                    if (UserInput.LeftDown) x.Component.PointerDown(mouseLocation);
                }
            }));


            if (UserInput.LeftClick)
            {
                bool flag = false;

                //Unfocus all components
                m_GuiComponentTree.Map((x =>
                {
                    x.Component.Focus = false;

                    //If component is clicked, then set it to be focuesd
                    if (x.Component.GetClickableBounds().Contains(mouseLocation) && !flag)
                    {
                        x.Component.PointerClick(mouseLocation);

                        //Focus all parents
                        foreach (var n in m_GuiComponentTree.GetParents(x))
                        {
                            if (n.Component != null) n.Component.Focus = true;
                        }

                        flag = true;
                    }
                }));
            }
        }

        public void DeleteComponent(IGuiComponent component)
        {
            m_GuiComponentTree.DeleteComponent(component);
        }
    }
}
