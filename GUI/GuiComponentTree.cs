﻿using System.Collections.Generic;
using System.Linq;
using System;

namespace GUI
{
    /// <summary>
    /// Tree representing parent-child relationships between
    /// components. Can be used to determine the draw-order
    /// </summary>
    public class GuiComponentTree
    {
        public GuiComponentTreeNode SuperNode { get; }

        public GuiComponentTree()
        {
            SuperNode = new GuiComponentTreeNode(null);
        }

        /// <summary>
        /// Gets all the parents of the given node
        /// </summary>
        /// <returns></returns>
        public IReadOnlyCollection<GuiComponentTreeNode> GetParents(GuiComponentTreeNode node)
        {
            var currentNode = node;
            var nodeList = new List<GuiComponentTreeNode>();

            while (currentNode.Component != null)
            {
                currentNode = FindParent(currentNode);
                nodeList.Add(currentNode);
            }

            return nodeList;
        }

        public IReadOnlyCollection<IGuiComponent> GetDrawOrder()
        {
            return GetDrawOrder(SuperNode);
        }

        /// <summary>
        /// BFS for building draw order
        /// </summary>
        /// <param name="node">Node to start</param>
        /// <returns>Order in which to draw GUI components</returns>
        public IReadOnlyCollection<IGuiComponent> GetDrawOrder(GuiComponentTreeNode node)
        {
            var drawOrder = new List<IGuiComponent>();
            Map(x => drawOrder.Add(x.Component));

            return drawOrder;
        }

        /// <summary>
        /// Add a component to the application with no parent
        /// </summary>
        public void AddComponent(IGuiComponent component)
        {
            SuperNode.ChildNodes.Add(new GuiComponentTreeNode(component));
        }

        /// <summary>
        /// Add a child to a component
        /// </summary>
        public void AddChild(IGuiComponent parent, IGuiComponent child)
        {
            var parentNode = FindNode(x => x.Component == parent);
            var childNode = FindNode(x => x.Component == child);

            AddChild(parentNode, childNode);
        }

        private void AddChild(GuiComponentTreeNode parent, GuiComponentTreeNode child)
        {
            //All components must be added at the 'base' level first
            //After that they can be assigned as children only ONCE.
            //Child node must also not contain parent.
            if (SuperNode.ChildNodes.Contains(child) &&
                !child.ChildNodes.Contains(parent))
            {
                SuperNode.ChildNodes.Remove(child);

                parent.ChildNodes.Add(child);
            }
        }

        /// <summary>
        /// Remove a component, destroying all it's children along with it
        /// </summary>
        /// <param name="component"></param>
        public void DeleteComponent(IGuiComponent component)
        {
            var node = FindNode(x => x.Component == component);
            var parentNode = FindParent(node);

            parentNode.ChildNodes.Remove(node);
        }

        /// <summary>
        /// Find the parent of a node
        /// </summary>
        public GuiComponentTreeNode FindParent(GuiComponentTreeNode node)
        {
            var parent = SuperNode;
            var foundParent = false;

            Map(x =>
            {
                if (!foundParent)
                {
                    foreach(var n in x.ChildNodes)
                    {
                        if(n == node)
                        {
                            parent = x;
                            foundParent = true;
                        }
                    }
                }
            });

            return parent;
        }

        /// <summary>
        /// Find a node with the specific query
        /// </summary>
        public GuiComponentTreeNode FindNode(Func<GuiComponentTreeNode, bool> query)
        {
            var openNodes = new Queue<GuiComponentTreeNode>();
            var node = SuperNode;

            openNodes.Enqueue(node);

            while (openNodes.Count > 0)
            {
                node = openNodes.Dequeue();

                foreach (var n in node.ChildNodes)
                {
                    if(query(n))
                    {
                        return n;
                    }

                    openNodes.Enqueue(n);
                }
            }

            return null;
        }

        /// <summary>
        /// Apply a function over the whole tree
        /// </summary>
        public GuiComponentTreeNode Map(Action<GuiComponentTreeNode> mapFunction)
        {
            var openNodes = new Queue<GuiComponentTreeNode>();
            var node = SuperNode;

            openNodes.Enqueue(node);

            while (openNodes.Count > 0)
            {
                node = openNodes.Dequeue();

                foreach (var n in node.ChildNodes)
                {
                    mapFunction(n);
                    openNodes.Enqueue(n);
                }
            }

            return null;
        }
    }


    public class GuiComponentTreeNode
    {
        public IGuiComponent Component { get; }
        public List<GuiComponentTreeNode> ChildNodes { get; set; }

        public GuiComponentTreeNode(IGuiComponent component, params GuiComponentTreeNode[] children)
        {
            Component = component;
            ChildNodes = children.ToList();
        }
    }
}
