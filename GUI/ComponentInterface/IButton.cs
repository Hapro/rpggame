﻿namespace GUI.ComponentInterface
{
    public interface IButton : ILabel
    {
        bool Pressed { get; set; }
    }
}
