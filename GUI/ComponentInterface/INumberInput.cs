﻿namespace GUI.ComponentInterface
{
    public interface INumberInput : ILabel
    {
        int NumberValue { get; set; }
    }
}
