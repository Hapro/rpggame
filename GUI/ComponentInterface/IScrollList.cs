﻿using System.Collections.Generic;

namespace GUI.ComponentInterface
{
    /// <summary>
    /// List of items, which has a scroll bar if
    /// the number of items is too large
    /// </summary>
    public interface IScrollList : IGuiComponent
    {
        List<string> Items { get; }

        string SelectedItem { get; }

        void ScrollDown();

        void ScrollUp();

        void AddItem(string item);
    }
}
