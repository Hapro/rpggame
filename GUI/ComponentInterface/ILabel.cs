﻿using Graphics;

namespace GUI.ComponentInterface
{
    public interface ILabel : IGuiComponent
    {
        string Text { get; set; }
    }
}
