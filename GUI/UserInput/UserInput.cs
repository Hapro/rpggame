﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace GUI.UserInput
{
    public delegate void KeyPressed(IReadOnlyCollection<Keys> keys);

    public class UserInput
    {
        private KeyboardState m_OldKeyboardState;
        private MouseState m_OldMouseState;

        public IReadOnlyCollection<Keys> KeysPressed { get; private set; }
        public IReadOnlyCollection<Keys> KeysStruck { get; private set; }

        public bool LeftClick { get; private set; }
        public bool RightClick { get; private set; }

        public bool LeftDown { get; private set; }
        public bool RightDown { get; private set; }

        public Vector2 MouseLocation { get; private set; }

        public event KeyPressed KeyPressed;

        public UserInput()
        {
            m_OldKeyboardState = Keyboard.GetState();
        }

        public void Update()
        {
            var newKeyboardState = Keyboard.GetState();
            var newPressedKeys = newKeyboardState.GetPressedKeys();
            var oldPressedKeys = m_OldKeyboardState.GetPressedKeys();
            var newStruckKeys = new List<Keys>();
            var newMouseState = Mouse.GetState();

            foreach(var key in newPressedKeys)
            {
                if(!oldPressedKeys.Contains(key) && newPressedKeys.Contains(key))
                {
                    newStruckKeys.Add(key);
                }
            }

            LeftClick = newMouseState.LeftButton == ButtonState.Pressed && m_OldMouseState.LeftButton == ButtonState.Released;
            RightClick = newMouseState.LeftButton == ButtonState.Pressed && m_OldMouseState.LeftButton == ButtonState.Released;

            LeftDown = newMouseState.LeftButton == ButtonState.Pressed;
            RightDown = newMouseState.RightButton == ButtonState.Pressed;

            MouseLocation = new Vector2(
                newMouseState.X,
                newMouseState.Y);

            KeysPressed = newPressedKeys;
            KeysStruck = newStruckKeys;
            m_OldKeyboardState = newKeyboardState;
            m_OldMouseState = newMouseState;

            if (newStruckKeys.Count > 0) KeyPressed?.Invoke(newStruckKeys);
        }

        public bool Left() { return KeysStruck.Contains(Keys.Left); }
        public bool Right() { return KeysStruck.Contains(Keys.Right); }
        public bool Up() { return KeysStruck.Contains(Keys.Up); }
        public bool Down() { return KeysStruck.Contains(Keys.Down); }
    }
}
