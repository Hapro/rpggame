﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Events
{
    public delegate void OnPointerHover();

    public delegate void OnPointerClick();

    public delegate void OnTextInput();

    public delegate void OnPointerDown();
}
