﻿using Graphics;
using Microsoft.Xna.Framework;
using GUI.Events;
using System;
using System.Collections.Generic;
using GUI.Factories;

namespace GUI
{
    public class GuiComponentEvents
    {
        public event OnPointerDown PointerIsDown;
        public event OnPointerClick PointerClicked;
        public event OnPointerHover PointerHovering;

        public Dictionary<Type, Action> m_Events;
        public Dictionary<Type, bool> m_EventsEnabled;

        public GuiComponentEvents()
        {
            m_Events = new Dictionary<Type, Action>();
            m_EventsEnabled = new Dictionary<Type, bool>();

            m_EventsEnabled.Add(typeof(OnPointerDown), true);
            m_EventsEnabled.Add(typeof(OnPointerClick), true);
            m_EventsEnabled.Add(typeof(OnPointerHover), true);

            m_Events.Add(typeof(OnPointerDown), () => 
            {
                if (m_EventsEnabled[typeof(OnPointerDown)])
                    PointerIsDown?.Invoke();
            });

            m_Events.Add(typeof(OnPointerClick), () =>
            {
                if (m_EventsEnabled[typeof(OnPointerClick)])
                    PointerClicked?.Invoke();
            });

            m_Events.Add(typeof(OnPointerHover), () =>
            {
                if (m_EventsEnabled[typeof(OnPointerHover)])
                    PointerHovering?.Invoke();
            });
        }

        public void EnableEvent(Type eventType)
        {
            m_EventsEnabled[eventType] = true;
        }

        public void DisableEvent(Type eventType)
        {
            m_EventsEnabled[eventType] = false;
        }

        public void EnableAllEvents()
        {
            foreach(var kvp in m_Events) EnableEvent(kvp.Key);
        }

        public void DisableAllEvents()
        {
            foreach (var kvp in m_Events) DisableEvent(kvp.Key);
        }

        public void FireEvent(Type eventType)
        {
            //This is Invoking the action which invokes the event,
            //not the event itself
            m_Events[eventType]?.Invoke();
        }
    }

    public abstract class GuiComponent : IGuiComponent
    {
        private readonly AnimatedSpriteGraphicsComponent m_GraphicsComponent;
        protected readonly IGuiComponentFactory m_GuiComponentFactory;
        protected bool m_Hidden;

        public GuiComponentEvents GuiComponentEvents { get; }

        public int OnPointerClickFrame { get; set; }
        public int OnPointerHoverFrame { get; set; }
        public int OnPointerDownFrame { get; set; }
        public int OnFocusFrame { get; set; }

        public bool Focus { get; set; }
        public int Width { get; }
        public int Height { get; }
        public Vector2 Location { get { return m_GraphicsComponent.GetLocation(); } }

        protected List<IGuiComponent> SubComponents;

        public GuiComponent(AnimatedSpriteGraphicsComponent graphicsComponent,
            IGuiComponentFactory guiComponentFactory,
            int width, int height)
        {
            GuiComponentEvents = new GuiComponentEvents();
            SubComponents = new List<IGuiComponent>();
            m_GuiComponentFactory = guiComponentFactory;
            m_GraphicsComponent = graphicsComponent;
            Width = width;
            Height = height;

            m_GraphicsComponent.Animate = false;

            OnPointerClickFrame = 0;
            OnPointerDownFrame = 0;
            OnPointerHoverFrame = 0;
            OnFocusFrame = 0;
        }

        public virtual void Hide()
        {
            GuiComponentEvents.DisableAllEvents();
            m_Hidden = true;

            foreach (var c in SubComponents) c.Hide();
        }

        public virtual void Show()
        {
            GuiComponentEvents.EnableAllEvents();
            m_Hidden = false;

            foreach (var c in SubComponents) c.Show();
        }


        public Vector2 GetCenter()
        {
            return new Vector2(Width / 2, Height / 2) + m_GraphicsComponent.GetLocation();
        }

        public virtual void Update(float elapsed)
        {
            m_GraphicsComponent.Update(elapsed);
            foreach (var c in SubComponents) c.Update(elapsed);
        }

        public virtual void Draw()
        {
            if (!m_Hidden)
            {
                m_GraphicsComponent.FrameIndex = Focus ? OnFocusFrame : m_GraphicsComponent.FrameIndex;
                m_GraphicsComponent.Draw();
                //Reset state of component
                m_GraphicsComponent.FrameIndex = Focus ? OnFocusFrame : 0;

                foreach (var c in SubComponents) c.Draw();
            }
        }

        public virtual void SetLocation(Vector2 location)
        {
            m_GraphicsComponent.SetLocation(location);

            foreach (var c in SubComponents)
            {
                //Sub components location is relative to
                //it's containing component
                c.SetLocation(c.GetLocation() + location);
            }
        }

        public void ToggleFocus(bool focus)
        {
            Focus = focus;
        }

        public Rectangle GetClickableBounds()
        {
            return new Rectangle(
                (int)m_GraphicsComponent.GetLocation().X,
                (int)m_GraphicsComponent.GetLocation().Y,
                Width,
                Height);
        }

        public virtual void TextInput(char letter)
        {
            foreach (var c in SubComponents) c.TextInput(letter);
        }

        public virtual void PointerHover(Vector2 location)
        {
            m_GraphicsComponent.FrameIndex = OnPointerHoverFrame;
            GuiComponentEvents.FireEvent(typeof(OnPointerHover));

            foreach(var c in SubComponents)
            {
                if (c.GetClickableBounds().Contains(location)) c.PointerHover(location);
            }
        }

        public virtual void PointerClick(Vector2 location)
        {
            Focus = true;
            m_GraphicsComponent.FrameIndex = OnPointerClickFrame;
            GuiComponentEvents.FireEvent(typeof(OnPointerClick));


            foreach (var c in SubComponents)
            {
                if (c.GetClickableBounds().Contains(location)) c.PointerClick(location);
            }
        }

        public virtual void PointerDown(Vector2 location)
        {
            m_GraphicsComponent.FrameIndex = OnPointerDownFrame;
            GuiComponentEvents.FireEvent(typeof(OnPointerDown));

            foreach (var c in SubComponents)
            {
                if (c.GetClickableBounds().Contains(location)) c.PointerDown(location);
            }
        }

        public Vector2 GetLocation()
        {
            return Location;
        }
    }
}
