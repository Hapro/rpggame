﻿using Microsoft.Xna.Framework;
using GUI.UserInput;

namespace GUI
{
    public interface IGuiManager
    {
        UserInput.UserInput UserInput { get; }

        void AddComponent(IGuiComponent component);

        void AddChild(IGuiComponent component, IGuiComponent parentComponent);

        void Update(float elapsed);

        void Draw();

        void DeleteComponent(IGuiComponent component);
    }
}
