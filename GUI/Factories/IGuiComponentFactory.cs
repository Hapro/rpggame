﻿using Graphics;
using GUI.ComponentInterface;
using Microsoft.Xna.Framework;

namespace GUI.Factories
{
    public interface IGuiComponentFactory
    {
        IWindow CreateWindow(Vector2 location, int width, int height, Color color);

        ITextInput CreateTextInput(Vector2 location, int width, int height, Color color);

        INumberInput CreateNumberInput(Vector2 location, int width, int height, Color color);

        IButton CreateButton(Vector2 location, int width, int height, Color color, string text);

        IScrollList CreateScrollList(Vector2 location, int width, int height, Color color, params string[] items);

        ILabel CreateLabel(Vector2 location, int width, int height, Color color, string text);

        GuiGrid CreateGrid();
    }
}
