﻿using Graphics;
using Microsoft.Xna.Framework.Content;
using GUI.ComponentInterface;
using Microsoft.Xna.Framework;

namespace GUI.Factories
{
    public abstract class GuiComponentFactory : IGuiComponentFactory
    {
        protected readonly GraphicsHandler m_GraphicsHandler;
        protected readonly ContentManager m_ContentManager;
        protected readonly IGuiManager m_GuiManager;

        public GuiComponentFactory(GraphicsHandler graphicsHandler, ContentManager contentManager, IGuiManager guiManager)
        {
            m_GraphicsHandler = graphicsHandler;
            m_ContentManager = contentManager;
            m_GuiManager = guiManager;
        }

        public abstract IWindow CreateWindow(Vector2 location, int width, int height, Color color);
        public abstract ITextInput CreateTextInput(Vector2 location, int width, int height, Color color);
        public abstract INumberInput CreateNumberInput(Vector2 location, int width, int height, Color color);
        public abstract IButton CreateButton(Vector2 location, int width, int height, Color color, string text);
        public abstract GuiGrid CreateGrid();
        public abstract IScrollList CreateScrollList(Vector2 location, int width, int height, Color color, params string[] items);
        public abstract ILabel CreateLabel(Vector2 location, int width, int height, Color color, string text);
    }
}
