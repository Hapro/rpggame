﻿using Graphics;
using GUI.Components;
using GUI.ComponentInterface;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace GUI.Factories
{
    public class GuiComponentFactoryDesktop : GuiComponentFactory
    {
        private readonly SpriteFont defaultFont;

        public GuiComponentFactoryDesktop(GraphicsHandler graphicsHandler,
            ContentManager contentManager, IGuiManager guiManager)
            : base(graphicsHandler, contentManager, guiManager)
        {
            defaultFont = contentManager.Load<SpriteFont>("DefaultFont");
        }

        private TextGraphicsComponent CreateTextGraphicsComponent()
        {
            return new TextGraphicsComponent(m_GraphicsHandler,
                "",
                defaultFont,
                Vector2.Zero
                );
        }
        public override IWindow CreateWindow(Vector2 location, int width, int height, Color color)
        {
            var graphic = new PrimitiveGraphicsComponent(
                PrimitiveGraphicsObjectFactory.CreateSquare(m_GraphicsHandler,
                new Vector2(width, height),
                color),
                m_GraphicsHandler,
                location);

            var window = new WindowGuiComponent(graphic.ToAnimated(),
                this,
                width,
                height);

            return window;
        }

        public override ITextInput CreateTextInput(Vector2 location, int width, int height, Color color)
        {
            var arr = new ScaledSpriteGraphicsComponent[]
            {
                new ScaledSpriteGraphicsComponent(m_GraphicsHandler, m_ContentManager.Load<Texture2D>("textField"), location, width, height),
                new ScaledSpriteGraphicsComponent(m_GraphicsHandler, m_ContentManager.Load<Texture2D>("textFieldSelected"), location, width, height),
            };

            var graphics = new AnimatedSpriteGraphicsComponent(
                m_GraphicsHandler,
                location,
                arr);

            var textInput = new TextInputGuiComponent(graphics,
                this,
                width,
                height);

            textInput.OnFocusFrame = 1;

            return textInput;
        }

        public override INumberInput CreateNumberInput(Vector2 location, int width, int height, Color color)
        {
            var arr = new ScaledSpriteGraphicsComponent[]
            {
                new ScaledSpriteGraphicsComponent(m_GraphicsHandler, m_ContentManager.Load<Texture2D>("textField"), location, width, height),
                new ScaledSpriteGraphicsComponent(m_GraphicsHandler, m_ContentManager.Load<Texture2D>("textFieldSelected"), location, width, height),
            };

            var graphics = new AnimatedSpriteGraphicsComponent(
                m_GraphicsHandler,
                location,
                arr);

            var numberComponent = new NumberGuiComponent(graphics,
                this,
                width,
                height);

            numberComponent.OnFocusFrame = 1;

            return numberComponent;
        }

        public override IButton CreateButton(Vector2 location, int width, int height, Color color, string text)
        {
            var arr = new ScaledSpriteGraphicsComponent[]
            {
                new ScaledSpriteGraphicsComponent(m_GraphicsHandler, m_ContentManager.Load<Texture2D>("buttonUp"), location, width, height),
                new ScaledSpriteGraphicsComponent(m_GraphicsHandler, m_ContentManager.Load<Texture2D>("buttonHover"), location, width, height),
                new ScaledSpriteGraphicsComponent(m_GraphicsHandler, m_ContentManager.Load<Texture2D>("buttonDown"), location, width, height),
            };

            var buttonGraphic = new AnimatedSpriteGraphicsComponent(m_GraphicsHandler, location, arr);

            var buttonComponent = new ButtonComponent(buttonGraphic,
                this,
                width,
                height);

            buttonComponent.Text = text;

            buttonComponent.OnPointerClickFrame = 2;
            buttonComponent.OnPointerHoverFrame = 1;
            buttonComponent.OnPointerDownFrame = 2;

            return buttonComponent;
        }

        public override GuiGrid CreateGrid()
        {
            var grid = new GuiGrid();
            grid.Unit = 40;

            return grid;
        }

        public override IScrollList CreateScrollList(Vector2 location, int width, int height,
            Color color, params string[] items)
        {
            var backgroundGraphic = new PrimitiveGraphicsComponent(
                PrimitiveGraphicsObjectFactory.CreateSquare(
                    m_GraphicsHandler, new Vector2(width, height), color),
                m_GraphicsHandler,
                location);

            var list = new ScrollList(backgroundGraphic.ToAnimated(),
                this,
                width,
                height);

            foreach (string s in items) list.AddItem(s);

            return list;
        }

        public override ILabel CreateLabel(Vector2 location, int width, int height, Color color, string text)
        {
            var graphic = new ScaledSpriteGraphicsComponent(m_GraphicsHandler,
                m_ContentManager.Load<Texture2D>("label"), location, width, height);
            var textGraphics = new TextGraphicsComponent(
                    m_GraphicsHandler,
                    text,
                    defaultFont,
                    location);

            var label = new LabelComponent(
                textGraphics,
                graphic.ToAnimated(),
                this,
                width,
                height);

            label.Text = text;

            return label;
        }
    }
}
