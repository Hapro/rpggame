﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Graphics
{
    public class PrimitiveGraphicsObject
    {
        private readonly VertexBuffer m_VertexBuffer;
        private readonly WorldViewProjection m_WorldViewProjection;
        private readonly int m_NumberOfPrimitives;
        private readonly BasicEffect m_BasicEffect;

        public PrimitiveGraphicsObject(VertexBuffer vertexBuffer, BasicEffect basicEffect,
            WorldViewProjection wvp, int numberOfPrimitives)
        {
            m_WorldViewProjection = wvp;

            m_NumberOfPrimitives = numberOfPrimitives;
            m_BasicEffect = basicEffect;
            m_VertexBuffer = vertexBuffer;

            UpdateDrawSettings();
        }

        public void Draw(GraphicsHandler graphicsHandler)
        {
            graphicsHandler.GraphicsDevice.SetVertexBuffer(m_VertexBuffer);

            foreach (EffectPass pass in m_BasicEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                graphicsHandler.GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, 2);
            }
        }

        public void SetLocation(Vector3 location)
        {
            m_WorldViewProjection.World = Matrix.CreateTranslation(location);
            UpdateDrawSettings();
        }

        private void UpdateDrawSettings()
        {
            m_BasicEffect.World = m_WorldViewProjection.World;
            m_BasicEffect.View = m_WorldViewProjection.View;
            m_BasicEffect.Projection = m_WorldViewProjection.Projection;
            m_BasicEffect.VertexColorEnabled = true;
        }

        public void ChangeEffectTechnique(string name)
        {
            m_BasicEffect.CurrentTechnique = m_BasicEffect.Techniques[name];
        }
    }
}
