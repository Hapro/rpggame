﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Graphics
{
    public static class PrimitiveGraphicsObjectFactory
    {
        private static WorldViewProjection CreateWorldViewProjection(GraphicsHandler graphicsHandler)
        {
            var viewportWidth = graphicsHandler.GraphicsDeviceManager.PreferredBackBufferWidth;
            var viewportHeight = graphicsHandler.GraphicsDeviceManager.PreferredBackBufferHeight;

            //http://www.bit-101.com/blog/?p=2832
            return new WorldViewProjection(
                Matrix.Identity,
                Matrix.Identity,
                Matrix.CreateOrthographicOffCenter
                    (0,
                    viewportWidth,     
                    viewportHeight,
                    0,  
                    0,
                    1)
                );          
        }

        public static PrimitiveGraphicsObject CreateSquare(GraphicsHandler graphicsHandler, Vector2 dimensions, Color color)
        {
            var basicEffect = new BasicEffect(graphicsHandler.GraphicsDevice);

            var x = dimensions.X;
            var y = dimensions.Y;
            var wvp = CreateWorldViewProjection(graphicsHandler);

            VertexPositionColor[] vertices = new VertexPositionColor[6];
            vertices[0] = new VertexPositionColor(new Vector3(0, 0, 0), color);
            vertices[1] = new VertexPositionColor(new Vector3(x, 0, 0), color);
            vertices[2] = new VertexPositionColor(new Vector3(x, y, 0), color);
            vertices[3] = new VertexPositionColor(new Vector3(x, y, 0), color);
            vertices[4] = new VertexPositionColor(new Vector3(0, y, 0), color);
            vertices[5] = new VertexPositionColor(new Vector3(0, 0, 0), color);

            var vertexBuffer = new VertexBuffer(graphicsHandler.GraphicsDevice,
                typeof(VertexPositionColor),
                6,
                BufferUsage.WriteOnly);

            vertexBuffer.SetData<VertexPositionColor>(vertices);

            return new PrimitiveGraphicsObject(vertexBuffer, basicEffect, wvp, 6);
        }

        private static void TransformVerticesToClipSpace(GraphicsHandler graphicsHandler, ref VertexPositionColor[] vertices)
        {
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i].Position = graphicsHandler.ScreenSpaceToClipSpace
                (
                    new Vector2
                    (
                        vertices[i].Position.X,
                        vertices[i].Position.Y
                    )
                );
            }
        }
    }
}
