﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Graphics
{
    public enum Align
    {
        Center,
        Left
    }

    public class TextGraphicsComponent : GraphicsComponent
    {
        public string Text { get; private set; }
        public Align Align { get; set; }
        public Color Color { get; set; }
        private readonly SpriteFont m_SpriteFont;
        private Vector2 m_Offset;

        public TextGraphicsComponent(GraphicsHandler graphicsHandler, string text, SpriteFont spriteFont, Vector2 location)
            : base(graphicsHandler, location)
        {
            Text = text;
            m_SpriteFont = spriteFont;
            Align = Align.Left;
            m_Offset = Vector2.Zero;
            Color = Color.Black;
        }

        private void CalculateNewDrawLocation()
        {
            if (Align == Align.Center)
            {
                var size = m_SpriteFont.MeasureString(Text);
                m_Offset = new Vector2(-size.X / 2, -size.Y / 2);
            }
        }

        private Vector2 GetDrawLocation()
        {
            return m_Location + m_Offset;
        }

        public override void Draw()
        {
            m_GraphicsHandler.SpriteBatch.DrawString(m_SpriteFont, Text, GetDrawLocation(), Color);
        }

        public void SetString(string values)
        {
            Text = "";

            foreach(var c in values)
            {
                AddChar(c);
            }
        }

        public void AddChar(char value)
        {
            if (m_SpriteFont.GetGlyphs().ContainsKey(value)) Text += value;

            CalculateNewDrawLocation();
        }

        public void RemoveChar()
        {
            if(Text.Length > 0) Text = Text.Remove(Text.Length - 1);


            CalculateNewDrawLocation();
        }
    }
}
