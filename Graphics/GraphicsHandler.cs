﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Graphics
{
    /// <summary>
    /// Class which allows classes to access objects
    /// required to draw things such as spritebatch
    /// </summary>
    public class GraphicsHandler
    {
        public GraphicsDeviceManager GraphicsDeviceManager { get; }
        
        public SpriteBatch SpriteBatch { get; }

        public GraphicsDevice GraphicsDevice { get; }

        public GraphicsHandler(GraphicsDeviceManager graphicsDeviceManager, SpriteBatch spriteBatch)
        {
            GraphicsDeviceManager = graphicsDeviceManager;
            GraphicsDevice = GraphicsDeviceManager.GraphicsDevice;
            SpriteBatch = spriteBatch;

            var rasterizerState = new RasterizerState();
            rasterizerState.CullMode = CullMode.None;
            GraphicsDevice.RasterizerState = rasterizerState;
        }

        public void SetGraphicsDeviceRasterizerState(RasterizerState state)
        {
            GraphicsDevice.RasterizerState = state;
        }

        public Vector3 ScreenSpaceToClipSpace(Vector2 screenLoc)
        {
            var backBufferWidth = GraphicsDevice.Adapter.CurrentDisplayMode.Width;
            var backBufferHeight = GraphicsDevice.Adapter.CurrentDisplayMode.Height;

            return new Vector3(screenLoc.X / backBufferWidth * 2.0f - 1.0f,
                screenLoc.Y / backBufferHeight * -2.0f + 1.0f, 0);
        }
    }
}
