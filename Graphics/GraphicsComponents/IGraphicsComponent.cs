﻿using Microsoft.Xna.Framework;

namespace Graphics
{
    public interface IGraphicsComponent
    {
        Vector2 GetLocation();

        void SetLocation(Vector2 location);

        void Draw();

        void Update(float elapsed);

        void SetColor(Color color);
    }
}
