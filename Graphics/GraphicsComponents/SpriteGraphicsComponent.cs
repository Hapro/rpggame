﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Graphics
{
    public class SpriteGraphicsComponent : GraphicsComponent
    {
        private Texture2D m_Texture;

        public SpriteGraphicsComponent(GraphicsHandler graphicsHandler, 
            Texture2D texture, Vector2 location)
            : base(graphicsHandler, location)
        {
            m_Texture = texture;
        }

        public override void Draw()
        {
            m_GraphicsHandler.SpriteBatch.Draw(m_Texture, m_Location, m_Color);
        }
    }
}
