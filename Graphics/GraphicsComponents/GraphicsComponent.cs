﻿using System;
using Microsoft.Xna.Framework;

namespace Graphics
{
    public abstract class GraphicsComponent : IGraphicsComponent
    {
        protected readonly GraphicsHandler m_GraphicsHandler;
        protected Vector2 m_Location;
        protected Color m_Color;

        public GraphicsComponent(GraphicsHandler graphicsHandler, Vector2 location)
        {
            m_GraphicsHandler = graphicsHandler;
            m_Location = location;
            m_Color = Color.White;
        }

        public abstract void Draw();

        public virtual Vector2 GetLocation()
        {
            return m_Location;
        }

        public virtual void SetColor(Color color)
        {
            m_Color = color;
        }

        public virtual void SetLocation(Vector2 location)
        {
            m_Location = location;
        }

        public virtual void Update(float elapsed) { }

        public AnimatedSpriteGraphicsComponent ToAnimated()
        {
            return new AnimatedSpriteGraphicsComponent(
                m_GraphicsHandler,
                m_Location,
                this);
        }
    }
}
