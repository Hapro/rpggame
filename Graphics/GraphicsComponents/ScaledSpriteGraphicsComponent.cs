﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Graphics
{
    public class ScaledSpriteGraphicsComponent : GraphicsComponent
    {
        private readonly Texture2D m_Texture;
        private readonly int m_Width;
        private readonly int m_Height;
       
        public ScaledSpriteGraphicsComponent(GraphicsHandler graphicsHandler,
            Texture2D texture, Vector2 location, int width, int height)
            : base(graphicsHandler, location)
        {
            m_Texture = texture;
            m_Height = height;
            m_Width = width;
        }

        public override void Draw()
        {
            m_GraphicsHandler.SpriteBatch.Draw(m_Texture, GetBounds(), null, m_Color);
        }

        public Rectangle GetBounds()
        {
            return new Rectangle(
                (int)m_Location.X,
                (int)m_Location.Y,
                m_Width,
                m_Height);
        }
    }
}
