﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace Graphics
{
    public class AnimatedSpriteGraphicsComponent : GraphicsComponent
    {
        private readonly IReadOnlyCollection<IGraphicsComponent> m_Frames;
        public int FrameIndex { get; set; }
        private float m_CurrentTime;
        public float Interval { get; set; }
        public bool Animate { get; set; }

        public AnimatedSpriteGraphicsComponent(GraphicsHandler graphicsHandler, Vector2 location,
            params IGraphicsComponent[] frames)
            : base(graphicsHandler, location)
        {
            FrameIndex = 0;
            m_Frames = frames;
            Interval = 1;
            m_CurrentTime = 0;
        }

        public override void SetLocation(Vector2 location)
        {
            foreach (var c in m_Frames) c.SetLocation(location);

            base.SetLocation(location);
        }

        public override void Draw()
        {
            m_Frames.ElementAt(FrameIndex).Draw();
        }

        public override void SetColor(Color color)
        {
            foreach (var c in m_Frames) c.SetColor(color);
            base.SetColor(color);
        }

        public override void Update(float elapsed)
        {
            if (Animate)
            {
                m_CurrentTime += elapsed;

                if (m_CurrentTime > Interval)
                {
                    m_CurrentTime = 0;
                    FrameIndex++;
                }

                if (FrameIndex >= m_Frames.Count)
                {
                    FrameIndex = 0;
                }
            }
        }
    }
}
