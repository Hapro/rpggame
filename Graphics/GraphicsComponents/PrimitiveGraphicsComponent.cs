﻿using Microsoft.Xna.Framework;

namespace Graphics
{
    public class PrimitiveGraphicsComponent : GraphicsComponent
    {
        private readonly PrimitiveGraphicsObject m_PrimitiveGraphicsObject;

        public PrimitiveGraphicsComponent(PrimitiveGraphicsObject primitiveGraphicsObject,
            GraphicsHandler graphicsHandler, Vector2 location)
            :base(graphicsHandler, location)
        {
            m_PrimitiveGraphicsObject = primitiveGraphicsObject;
            SetLocation(location);
        }

        public override void Draw()
        {
            m_PrimitiveGraphicsObject.Draw(m_GraphicsHandler);
        }

        public override void SetLocation(Vector2 location)
        {
            m_PrimitiveGraphicsObject.SetLocation(new Vector3(location.X, location.Y, 0));
            base.SetLocation(location);
        }
    }
}
