﻿using Microsoft.Xna.Framework;

namespace Graphics
{
    public class WorldViewProjection
    {
        public Matrix World { get; set; }
        public Matrix View { get; }
        public Matrix Projection { get; }

        public WorldViewProjection(Matrix world, Matrix view, Matrix projection)
        {
            World = world;
            View = view;
            Projection = projection;
        }
    }
}
