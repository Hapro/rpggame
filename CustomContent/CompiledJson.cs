﻿namespace CustomContentPipeline
{
    public class CompiledJson
    {
        public byte[] CompiledJsonString { get; }

        public CompiledJson(byte[] compiledJsonString)
        {
            CompiledJsonString = compiledJsonString;
        }
    }
}
