﻿using Microsoft.Xna.Framework.Content.Pipeline;
using System;
using System.IO;
using Utils;

namespace CustomContentPipeline
{
    [ContentImporter(".json", DefaultProcessor = "JsonProcessor", DisplayName = "Json importer")]
    public class JsonImporter : ContentImporter<JsonData>
    {
        public override JsonData Import(string filename, ContentImporterContext context)
        {
            context.Logger.LogMessage($"Importing Json file {filename}");

            try
            {
                return new JsonData(LoadFile(filename));
            }
            catch (Exception e)
            {
                context.Logger.LogMessage($"ERROR! Could not load Json file {filename}. Exception: {e.Message}");
                return new JsonData("ERR");
            }
        }

        private string LoadFile(string filename)
        {
            try
            {
                var fs = new StreamReader(filename);
                var str = "";

                while (!fs.EndOfStream) { str += fs.ReadLine(); }

                fs.Close();

                return str;
            }
            catch (IOException)
            {
                return null;
            }
        }
    }
}
