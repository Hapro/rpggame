﻿using Microsoft.Xna.Framework.Content.Pipeline;
using Utils;

namespace CustomContentPipeline
{
    [ContentProcessor(DisplayName = "Json Processor")]
    public class JsonProcessor : ContentProcessor<JsonData, CompiledJson>
    {
        public override CompiledJson Process(JsonData input, ContentProcessorContext context)
        {
            var arr = new byte[input.Data.Length];

            for (var i = 0; i < input.Data.Length; i++) arr[i] = (byte)input.Data[i];

            return new CompiledJson(arr);
        }
    }
}
