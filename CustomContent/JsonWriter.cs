﻿using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using Microsoft.Xna.Framework.Content.Pipeline;
using Utils;

namespace CustomContentPipeline
{
    [ContentTypeWriter]
    public class JsonWriter : ContentTypeWriter<CompiledJson>
    {
        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return typeof(JsonReader).AssemblyQualifiedName;
        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return typeof(CompiledJson).AssemblyQualifiedName;
        }

        protected override void Write(ContentWriter output, CompiledJson value)
        {
            output.Write(value.CompiledJsonString.Length);
            foreach (var d in value.CompiledJsonString) output.Write(d);
        }
    }
}
