﻿using System;
using TurnBasedBattle.Scripts.SkillScripts;
using Utils;

namespace TurnBasedBattle
{
    /// <summary>
    /// A skill is something like an attack, fireball etc
    /// </summary>
    public class Skill
    {
        public string Name { get; }
        public int Range { get; }
        public StatusEffect StatusEffect { get; }
        public SkillScript Script { get; }

        public Skill(string name, int range, StatusEffect statusEffect, SkillScript script)
        {
            Name = name;
            Range = range;
            StatusEffect = statusEffect;
            Script = script;
        }

        public virtual void AffectActor(BattleMapActor actor)
        {
            Script.SetParameter("Target", actor);

            Script.Run();

            if(StatusEffect != null) actor.AddStatusEffect(StatusEffect);
        }
    }
}
