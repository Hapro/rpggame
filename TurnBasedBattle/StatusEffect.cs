﻿using System;
using TurnBasedBattle.StatusEffectScripts.Scripts;

namespace TurnBasedBattle
{
    public class StatusEffect
    {
        public int Duration { get; private set; }
        public string Name { get; }
        private StatusEffectScript m_StatusEffectScript;

        public StatusEffect(string name, int duration)
        {
            Duration = duration;
            Name = name;
        }

        public void AffectActor(BattleMapActor actor)
        {
            m_StatusEffectScript.SetParameter("Actor", actor);
            m_StatusEffectScript.Run();
            Duration--;
        }
    }
}
