﻿using System;
using System.Collections.Generic;
using Utils;
using System.Linq;

namespace TurnBasedBattle
{
    public class BattleMap
    {
        public int Width { get; }
        public int Height { get; }
        public int TileSize { get;}
        public Camera Camera { get; }
        private readonly BattleMapTile[,] m_BattleMapTiles;

        public BattleMap(BattleMapTile[,] battleMapTiles, int width, int height, int tileSize)
        {
            m_BattleMapTiles = battleMapTiles;
            Width = width;
            Height = height;
            TileSize = tileSize;
            Camera = new Camera();
        }

        public BattleMapTile GetTileAt(int x, int y)
        {
            if(x >= 0 && y >= 0 && x < Width && y < Height)
            {
                return m_BattleMapTiles[x, y];
            }

            return null;
        }

        public void MapTiles(Action<BattleMapTile> func)
        {
            foreach (var tile in m_BattleMapTiles) func(tile);
        }

        public IReadOnlyCollection<BattleMapTile> GetAllTiles()
        {
            List<BattleMapTile> tiles = new List<BattleMapTile>(Width * Height);
            MapTiles(z => tiles.Add(z));
            return tiles;
        }

        /// <summary>
        /// Gets a 'square' of tiles, the center of which is the x and y co-ordinates
        /// </summary>
        public IReadOnlyCollection<BattleMapTile> GetRectangleOfTiles(int x, int y, int radius)
        {
            var arr = new List<BattleMapTile>();

            for (int xVal = x - radius; xVal <= x + radius; xVal++)
            {
                for (int yVal = y - radius; yVal <= y + radius; yVal++)
                {
                    var t = GetTileAt(xVal, yVal);
                    if (t != null) arr.Add(t);
                }
            }

            return arr;
        }

        public IReadOnlyCollection<BattleMapTile> GetAdjacentTilesTo(int x, int y)
        {
            return GetRectangleOfTiles(x, y, 1).Where(z => z.X != x && z.Y != y).ToArray(); 
        }
    }
}
