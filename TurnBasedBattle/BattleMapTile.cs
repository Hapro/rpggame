﻿using Graphics;
using System;
using Utils;

namespace TurnBasedBattle
{
    public class BattleMapTile : IPathFindingTile2D
    {
        public IGraphicsComponent GraphicsComponent { get; }
        public int X { get; }
        public int Y { get; }
        public bool Traversable { get; set; }
        public int Elevation { get; }

        public BattleMapTile(IGraphicsComponent graphicsComponent, bool traversable = true,
            int x = 0, int y = 0, int elevation = 0)
        {
            GraphicsComponent = graphicsComponent;
            X = x;
            Y = y;
            Elevation = elevation;
            Traversable = traversable;
        }

        public bool IsAdjacent(IPathFindingTile2D other)
        {
            if (other.X == X && other.Y == Y)
                return false;

            var xDist = Math.Abs(X - other.X);
            var yDist = Math.Abs(Y - other.Y);

            return xDist <= 1 && yDist <= 1;
        }

        public float CostFunction(IPathFindingTile2D tile)
        {
            if (!Traversable) return int.MaxValue;

            var xDist = Math.Abs(tile.X - X);
            var yDist = Math.Abs(tile.Y - Y);
            return xDist + yDist;
        }

        public float DistanceFrom(BattleMapTile otherTile)
        {
            var a = Math.Abs(otherTile.X - X);
            var b = Math.Abs(otherTile.Y - Y);

            return (float)Math.Sqrt((a * a) + (b * b));
        }
    }
}
