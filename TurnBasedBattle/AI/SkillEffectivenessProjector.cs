﻿using System;
using System.Collections.Generic;
using NClone;
namespace TurnBasedBattle.AI
{
    /// <summary>
    /// Class used to 'project' the effectivness of a given
    /// skill against a given target.
    /// </summary>
    public class SkillEffectivenessProjector
    {
        public Func<BattleMapActor, BattleMapActor, float> AttackHeuristicFunction { get; }
        public Func<BattleMapActor, BattleMapActor, float> HealHeuristicFunction { get; }

        public SkillEffectivenessProjector()
        {
            AttackHeuristicFunction = AttackHeuristic;
            HealHeuristicFunction = HealHeuristic;
        }

        /// <summary>
        /// Finds the most effective skill to use against the target. Ordered from most to least effective
        /// </summary>
        public IEnumerable<Skill> DetermineMostEffectiveSkillAgainst(IEnumerable<Skill> skills, BattleMapActor target,
            Func<BattleMapActor, BattleMapActor, float> heuristic)
        {
            var lst = new List<Tuple<float, Skill>>();

            foreach(var s in skills) lst.Add(Tuple.Create(DetermineEffectivenessOfSkillAgainst(s, target, heuristic), s));
            
            //Bubble sort
            for(var i = 0; i < lst.Count-1; i++)
            {
                for(var i2 = 0; i2 < lst.Count; i2++)
                {
                    if(lst[i].Item1 < lst[i + 1].Item1)
                    {
                        var t = lst[i];
                        lst[i] = lst[i+1];
                        lst[i + 1] = t;
                    }
                }
            }

            foreach (var s in lst) yield return s.Item2;
        }


        /// <summary>
        /// Looks at the skill, and how effective the status effect is to determine how to apply
        /// </summary>
        private float DetermineEffectivenessOfSkillAgainst(Skill skill, BattleMapActor target, 
            Func<BattleMapActor, BattleMapActor, float> heuristic)
        {
            var targetCopy = CloneBattleMapActor(target);

            skill.AffectActor(targetCopy);

            if (skill.StatusEffect != null)
            {
                var statusEffectCopy = CloneStatusEffect(skill.StatusEffect);

                while (statusEffectCopy.Duration > 0) statusEffectCopy.AffectActor(targetCopy);
            }

            return heuristic(target, targetCopy);
        }


        private float AttackHeuristic(BattleMapActor before, BattleMapActor after)
        {
            var damage = before.BattleMapCharacterProfile.CurrentHealth - after.BattleMapCharacterProfile.CurrentHealth;

            return damage;
        }

        private float HealHeuristic(BattleMapActor before, BattleMapActor after)
        {
            var healed = after.BattleMapCharacterProfile.CurrentHealth - before.BattleMapCharacterProfile.CurrentHealth;

            return healed;
        }

        private BattleMapActor CloneBattleMapActor(BattleMapActor actor)
        {
            return new BattleMapActor(
                Clone.ObjectGraph(actor.BattleMapCharacterProfile),
                null);
        }

        private StatusEffect CloneStatusEffect(StatusEffect effect)
        {
            return Clone.ObjectGraph(effect);
        }
    }
}
