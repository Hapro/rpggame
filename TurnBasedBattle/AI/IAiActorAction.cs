﻿namespace TurnBasedBattle.AI
{
    public interface IAiActorAction
    {
        void Attack(ITurnAction turnAction, int x, int y);

        void Move(ITurnMove turnMove, int x, int y);
    }
}
