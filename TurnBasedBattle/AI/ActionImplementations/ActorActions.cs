﻿using TurnBasedBattle.AI;

namespace TurnBasedBattle.ActionImplementations
{
    public abstract class ActorActions : IAiActorAction
    {
        public abstract void Attack(ITurnAction turnAction, int x, int y);

        public abstract void Move(ITurnMove turnMove, int x, int y);
    }
}
