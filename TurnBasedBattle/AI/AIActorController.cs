﻿using System;
using TurnBasedBattle.AI.AIStates;

namespace TurnBasedBattle.AI
{
    /// <summary>
    /// Controls a single BattleMapActor
    /// </summary>
    public class AiActorEngine
    {
        public BattleMapActor Actor { get; }

        public AiClassStateTransformer StateTransformer { get; }

        private AiActorState CurrentState { get; set; }

        private IAiActorAction ActorActions { get; }

        public AiActorEngine(BattleMapActor actor, TurnBasedBattleData turnBasedBattleData,
            AiClassStateTransformer stateTransformer, IAiActorAction actorActions)
        {
            ActorActions = actorActions;
            StateTransformer = stateTransformer;
            Actor = actor;
            //TODO: refactor this
            var startThing = new AiActorState(turnBasedBattleData, actorActions, Actor, new SkillEffectivenessProjector());
            var startState = stateTransformer.StartState;
            CurrentState = (AiActorState)Activator.CreateInstance(
                startState, startThing);
        }

        public void UpdateTurnMove(ITurnMove turnMove)
        {
            CurrentState.UpdateTurnMove(turnMove);
        }

        public void UpdateTurnAction(ITurnAction turnAction)
        {
            var newState = CurrentState.UpdateTurnAction(turnAction);

            if (newState != CurrentState)
            {
                CurrentState = newState;
                CurrentState.ChangeFunction = StateTransformer.States[newState.GetType()];
            }
        }
    }
}
