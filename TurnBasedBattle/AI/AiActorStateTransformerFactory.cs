﻿using System;
using System.Collections.Generic;
using TurnBasedBattle.AI.AIStates;

namespace TurnBasedBattle.AI
{
    public enum ActorClass
    {
        Warrior,
        Mage
    }

    /// <summary>
    /// Contains state transformers for each class
    /// </summary>
    public class AiClassStateTransformer
    {
        public Dictionary<Type, Func<AiActorState, AiActorState>> States { get; set; }
        public Type StartState { get; set; }

        public AiClassStateTransformer()
        {
            States = new Dictionary<Type, Func<AiActorState, AiActorState>>();
        }
    }

    public class AiActorStateTransformerFactory
    {
        public AiClassStateTransformer Generate(ActorClass actorClass)
        {
            switch (actorClass)
            {
                case ActorClass.Mage:
                    return GenerateMage();

                case ActorClass.Warrior:
                    return GenerateWarrior();
            }

            return null;
        }

        private AiClassStateTransformer GenerateWarrior()
        {
            var st = new AiClassStateTransformer();

            st.States.Add(
                typeof(AggressiveState),
                (z) =>
                {
                    return z;
                });

            st.StartState = typeof(AggressiveState);

            return st;
        }

        private AiClassStateTransformer GenerateMage()
        {
            var st = new AiClassStateTransformer();

            st.States.Add(
                typeof(RangedState),
                (z) =>
                {
                    return new AggressiveState(z);
                });

            st.StartState = typeof(AggressiveState);

            return st;
        }
    }
}
