﻿using System;

namespace TurnBasedBattle.AI.AIStates
{
    public class AiActorState
    {
        public Func<AiActorState, AiActorState> ChangeFunction { get; set; }
        protected IAiActorAction ActorActions { get; }
        protected BattleMapActor Actor { get; }
        protected readonly TurnBasedBattleData m_TurnBasedBattleData;
        public SkillEffectivenessProjector SkillEffectivenessProjector { get; }

        public AiActorState(TurnBasedBattleData turnBasedBattleData,
            IAiActorAction actorActions, BattleMapActor actor,
            SkillEffectivenessProjector skillEffectivenessProjector)
        {
            m_TurnBasedBattleData = turnBasedBattleData;
            ActorActions = actorActions;
            ChangeFunction = (z) => { return z; };
            Actor = actor;
            SkillEffectivenessProjector = skillEffectivenessProjector;
        }

        public AiActorState(AiActorState previousState)
        {
            m_TurnBasedBattleData = previousState.m_TurnBasedBattleData;
            ActorActions = previousState.ActorActions;
            Actor = previousState.Actor;
            ChangeFunction = (z) => { return z; };
            SkillEffectivenessProjector = previousState.SkillEffectivenessProjector;
        }

        public virtual void UpdateTurnMove(ITurnMove turnMove) { }

        public virtual AiActorState UpdateTurnAction(ITurnAction turnAction)
        {
            return ChangeFunction.Invoke(this);
        }
    }
}
