﻿using System;

namespace TurnBasedBattle.AI.AIStates
{

    public class RangedState : AiActorState
    {
        public RangedState(AiActorState previousState) : base(previousState)
        {
        }

        public override void UpdateTurnMove(ITurnMove turnMove)
        {
            throw new NotImplementedException();
        }

        public override AiActorState UpdateTurnAction(ITurnAction turnAction)
        {
            return base.UpdateTurnAction(turnAction);
        }
    }
}
