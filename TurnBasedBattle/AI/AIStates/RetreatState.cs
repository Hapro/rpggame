﻿using System;

namespace TurnBasedBattle.AI.AIStates
{
    public class RetreatState : AiActorState
    {
        public RetreatState(AiActorState previousState)
            : base(previousState)
        {
        }

        public override void UpdateTurnMove(ITurnMove turnMove)
        {
            throw new NotImplementedException();
        }

        public override AiActorState UpdateTurnAction(ITurnAction turnAction)
        {
            return base.UpdateTurnAction(turnAction);
        }
    }
}
