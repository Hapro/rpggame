﻿using System.Linq;

namespace TurnBasedBattle.AI.AIStates
{
    public class AggressiveState : AiActorState
    {
        public AggressiveState(AiActorState previousState)
            : base(previousState)
        {
        }

        public override void UpdateTurnMove(ITurnMove turnMove)
        {
            var targtActor = m_TurnBasedBattleData.PlayerParty.BattleMapActors.ElementAt(0);
            var targetActorTile = m_TurnBasedBattleData.BattleActorManager.GetTileActorIsOn(targtActor);
            var targetTiles = m_TurnBasedBattleData.BattleMap.GetAdjacentTilesTo(targetActorTile.X, targetActorTile.Y);
            var targetTile = targetTiles.First();

            turnMove.MoveActor(targetTile.X, targetTile.Y);
        }

        public override AiActorState UpdateTurnAction(ITurnAction turnAction)
        { 
            var target = m_TurnBasedBattleData.PlayerParty.BattleMapActors.ElementAt(0);
            var targetTile = m_TurnBasedBattleData.BattleActorManager.GetTileActorIsOn(target);
            var distance = targetTile.DistanceFrom(
                m_TurnBasedBattleData.BattleActorManager.GetTileActorIsOn(Actor));
            var skillsInRange = Actor.BattleMapCharacterProfile.KnownSkills.Where(x => x.Range >= distance);

            if(skillsInRange.Count() > 0)
            {
                var skillToUse = SkillEffectivenessProjector.DetermineMostEffectiveSkillAgainst(
                    skillsInRange,
                    target,
                    SkillEffectivenessProjector.AttackHeuristicFunction);

                turnAction.DoAction(skillToUse.ElementAt(0).Name, targetTile.X, targetTile.Y);
            }

            return base.UpdateTurnAction(turnAction);
        }
    }
}
