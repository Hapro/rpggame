﻿namespace TurnBasedBattle
{
    /// <summary>
    /// Contains all the immutable data for a turn based battle
    /// </summary>
    public class TurnBasedBattleData
    {
        public BattleMap BattleMap { get; }
        public BattleMapActorManager BattleActorManager { get; }
        public Party PlayerParty { get; }
        public Party OpponentParty { get; }
        public BattleMapActor ActiveActor { get; set; }

        public TurnBasedBattleData(BattleMap battleMap, BattleMapActorManager battleMapActorManager,
            Party playerParty, Party opponentParty, bool playerTurn)
        {
            BattleActorManager = battleMapActorManager;
            BattleMap = battleMap;
            PlayerParty = playerParty;
            OpponentParty = opponentParty;
        }
    }
}
