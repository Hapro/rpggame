﻿using System;
using System.Collections.Generic;

namespace TurnBasedBattle
{
    /// <summary>
    /// Class to keep a list of actors on the map,
    /// and the tiles they are on.
    /// </summary>
    public class BattleMapActorManager
    {
        private readonly Dictionary<BattleMapActor, BattleMapTile> m_ActorsOnTiles;
        public IReadOnlyCollection<BattleMapActor> ActorsOnMap { get; }

        public BattleMapActorManager(params BattleMapActor[] actors)
        {
            m_ActorsOnTiles = new Dictionary<BattleMapActor, BattleMapTile>();
            ActorsOnMap = actors;
        }

        public void PutActorOnTile(BattleMapActor actor, BattleMapTile tile)
        {
            if (IsTileEmpty(tile))
            {
                m_ActorsOnTiles.Remove(actor);
                m_ActorsOnTiles.Add(actor, tile);
            }
        }

        public BattleMapTile GetTileActorIsOn(BattleMapActor actor)
        {
            return m_ActorsOnTiles.ContainsKey(actor) ? m_ActorsOnTiles[actor] : null;
        }

        public bool IsTileEmpty(BattleMapTile tile)
        {
            return !m_ActorsOnTiles.ContainsValue(tile);
        }

        public void MapActors(Action<BattleMapActor> func)
        {
            foreach(var actor in ActorsOnMap)
            {
                func(actor);
            }
        }
    }
}
