﻿using System;
using System.Collections.Generic;
using TurnBasedBattle.AI;

namespace TurnBasedBattle
{
    public class Party
    {
        public string PartyName { get; }
        public IReadOnlyCollection<BattleMapActor> BattleMapActors { get; }

        public Party(params BattleMapActor[] actors)
        {
            BattleMapActors = actors;
        }
    }
}
