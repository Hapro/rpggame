﻿using Graphics;
using System.Collections.Generic;

namespace TurnBasedBattle
{
    public class BattleMapActor
    {
        public IGraphicsComponent GraphicsComponent { get; }
        public List<StatusEffect> StatusEffects { get; }
        public BattleMapCharacterProfile BattleMapCharacterProfile { get; }

        public BattleMapActor(BattleMapCharacterProfile characterProfile,
            IGraphicsComponent graphicsComponent)
        {
            GraphicsComponent = graphicsComponent;
            StatusEffects = new List<StatusEffect>();
            BattleMapCharacterProfile = characterProfile;
        }

        public void UpdateEndOfTurn()
        {
        }

        public void AddStatusEffect(StatusEffect statusEffect)
        {
            bool shouldAdd = true;

            foreach(var e in StatusEffects)
            {
                if (e.Name == statusEffect.Name) shouldAdd = false;
            }

            if (statusEffect.Duration <= 0) shouldAdd = false;

            if(shouldAdd) StatusEffects.Add(statusEffect);
        }
    }
}
