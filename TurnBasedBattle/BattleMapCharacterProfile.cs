﻿using CharacterData;
using System.Collections.Generic;

namespace TurnBasedBattle
{
    /// <summary>
    /// Wraps character profile info in battle-only data
    /// </summary>
    public class BattleMapCharacterProfile
    {
        public CharacterProfile CharacterProfile { get; }
        public IReadOnlyCollection<Skill> KnownSkills { get; }
        public float CurrentHealth { get; set; }

        public BattleMapCharacterProfile(CharacterProfile characterProfile,
            IReadOnlyCollection<Skill> knownSkills)
        {
            CharacterProfile = characterProfile;
            KnownSkills = knownSkills;
            CurrentHealth = characterProfile.CharacterStats.MaxHealth.Value;
        }
    }
}
