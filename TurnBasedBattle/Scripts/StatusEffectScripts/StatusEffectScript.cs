﻿using System;
using Utils;

namespace TurnBasedBattle.StatusEffectScripts.Scripts
{
    public class StatusEffectScript : Script
    {
        public StatusEffectScript(string name)
            : base(name)
        {
            RequireParameters
            (
                Tuple.Create("Actor", typeof(BattleMapActor))
            );
        }
    }
}
