﻿using System;

namespace TurnBasedBattle.StatusEffectScripts.Scripts
{
    public class PoisonScript : StatusEffectScript
    {
        public PoisonScript() 
            : base("Poison")
        {
            RequireParameters
            (
                Tuple.Create("DamagePerTurn", typeof(float))
            );

            m_Script = (x) =>
            {
                var actor = (BattleMapActor)x.GetParameter("Actor");
                var dpt = (float)x.GetParameter("DamagePerTurn");
                var stats = actor.BattleMapCharacterProfile.CurrentHealth -= dpt;
            };
        }
    }
}
