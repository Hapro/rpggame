﻿using Utils;

namespace TurnBasedBattle.Scripts.SkillScripts
{
    public class AttackSkillScript : SkillScript
    {
        public AttackSkillScript()
            : base("Attack")
        {
            m_Script = (x) =>
            {
                var target = (BattleMapActor)m_Params.GetParameter("Target");
                var user = (BattleMapActor)m_Params.GetParameter("User");
                target.BattleMapCharacterProfile.CurrentHealth -= user.BattleMapCharacterProfile.CharacterProfile.CharacterStats.CharacterAttributes.Str.Value;
            };
        }
    }
}
