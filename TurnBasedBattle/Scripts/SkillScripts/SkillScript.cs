﻿using System;
using Utils;

namespace TurnBasedBattle.Scripts.SkillScripts
{
    public class SkillScript : Script
    {
        public SkillScript(string name)
            : base(name)
        {
            RequireParameters(Tuple.Create("Target", typeof(BattleMapActor)));
            RequireParameters(Tuple.Create("User", typeof(BattleMapActor)));
        }
    }
}
