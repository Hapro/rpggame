﻿using TurnBasedBattle.StatusEffectScripts.Scripts;

namespace TurnBasedBattle.Scripts.Factories
{
    public class StatusEffectScriptFactory : ScriptFactory
    {
        public StatusEffectScriptFactory()
        {
            AddScript(new PoisonScript());
        }
    }
}
