﻿using TurnBasedBattle.Scripts.SkillScripts;

namespace TurnBasedBattle.Scripts.Factories
{
    public class SkillScriptFactory : ScriptFactory
    {
        public SkillScriptFactory()
        {
            AddScript(new AttackSkillScript());
        }
    }
}

