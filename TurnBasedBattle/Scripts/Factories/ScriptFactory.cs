﻿using System.Collections.Generic;
using Utils;

namespace TurnBasedBattle.Scripts.Factories
{
    public abstract class ScriptFactory
    {
        private Dictionary<string, Script> m_Scripts;

        public ScriptFactory()
        {
            m_Scripts = new Dictionary<string, Script>();
        }

        public void AddScript(Script s)
        {
            m_Scripts.Add(s.Name, s);
        }

        public Script GetScript(string effectName)
        {
            return m_Scripts.ContainsKey(effectName) ? m_Scripts[effectName] : new Script("No Script found");
        }
    }
}
