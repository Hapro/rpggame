﻿namespace TurnBasedBattle
{
    /// <summary>
    /// These interfaces define what it means to be a 'turn' 
    /// in a turn based battle
    /// </summary>
    public interface ITurnMove
    {
        bool MoveActor(int x, int y);
    }

    public interface ITurnAction
    {
        bool DoAction(string actionName, int x, int y);
    }
}
