﻿using TurnBasedBattle.AI;

namespace TurnBasedBattle
{
    public abstract class ActorController
    {
        public abstract void TurnMove(ITurnMove turnMove);

        public abstract void TurnAction(ITurnAction turnAction);

        public abstract void NewTurn();
    }

    /// <summary>
    /// Since this controller is waiting for gui input, it basically does nothing
    /// </summary>
    public class GuiActorController : ActorController
    {
        public override void NewTurn()
        {
        }

        public override void TurnAction(ITurnAction turnAction)
        {
        }

        public override void TurnMove(ITurnMove turnMove)
        {
        }
    }

    public class AiOpponentActorController : ActorController
    {
        private readonly AiActorEngine m_AiActorEngine;
        private bool moved;
        private bool acted;

        public AiOpponentActorController(AiActorEngine aiActorEngine)
        {
            m_AiActorEngine = aiActorEngine;
            moved = false;
            acted = false;
        }

        public override void TurnMove(ITurnMove turnMove)
        {
            if (!moved)
            {
                m_AiActorEngine.UpdateTurnMove(turnMove);
                moved = true;
            }
        }

        public override void TurnAction(ITurnAction turnAction)
        {
            if (!acted)
            {
                m_AiActorEngine.UpdateTurnAction(turnAction);
                acted = true;
            }
        }

        public override void NewTurn()
        {
            acted = false;
            moved = false;
        }
    }
}
