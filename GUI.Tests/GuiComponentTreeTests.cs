﻿using NUnit.Framework;
using System.Linq;
using Microsoft.Xna.Framework;
using System;
using GUI.Events;

namespace GUI.Tests
{
    public class GuiComponentTreeTests
    {
        public GuiComponentTree CreateTree()
        {
            var tree = new GuiComponentTree();
            var nodeA = new GuiComponentTreeNode(new FakeGuiComponent("A"));
            var nodeB = new GuiComponentTreeNode(new FakeGuiComponent("B"));
            var nodeC = new GuiComponentTreeNode(new FakeGuiComponent("C"));
            var nodeD = new GuiComponentTreeNode(new FakeGuiComponent("D"));
            var nodeE = new GuiComponentTreeNode(new FakeGuiComponent("E"));
            var nodeF = new GuiComponentTreeNode(new FakeGuiComponent("F"));

            nodeA.ChildNodes.Add(nodeB);
            nodeA.ChildNodes.Add(nodeC);
            nodeB.ChildNodes.Add(nodeD);
            nodeC.ChildNodes.Add(nodeE);
            nodeC.ChildNodes.Add(nodeF);

            tree.SuperNode.ChildNodes.Add(nodeA);

            return tree;
        }

        [Test]
        public void WhenValidComponents_ThenCanFindNodes()
        {
            var tree = new GuiComponentTree();
            var nodeA = new GuiComponentTreeNode(new FakeGuiComponent("A"));
            var nodeB = new GuiComponentTreeNode(new FakeGuiComponent("B"));
            var nodeC = new GuiComponentTreeNode(new FakeGuiComponent("C"));
            var nodeD = new GuiComponentTreeNode(new FakeGuiComponent("D"));
            var nodeE = new GuiComponentTreeNode(new FakeGuiComponent("E"));
            var nodeF = new GuiComponentTreeNode(new FakeGuiComponent("F"));

            nodeA.ChildNodes.Add(nodeB);
            nodeA.ChildNodes.Add(nodeC);
            nodeB.ChildNodes.Add(nodeD);
            nodeC.ChildNodes.Add(nodeE);
            nodeC.ChildNodes.Add(nodeF);

            tree.SuperNode.ChildNodes.Add(nodeA);

            Assert.That(tree.FindNode(x => x == nodeA), Is.EqualTo(nodeA));
            Assert.That(tree.FindNode(x => x == nodeB), Is.EqualTo(nodeB));
            Assert.That(tree.FindNode(x => x == nodeC), Is.EqualTo(nodeC));
            Assert.That(tree.FindNode(x => x == nodeD), Is.EqualTo(nodeD));
            Assert.That(tree.FindNode(x => x == nodeE), Is.EqualTo(nodeE));
            Assert.That(tree.FindNode(x => x == nodeF), Is.EqualTo(nodeF));
        }

        [Test]
        public void WhenValidComponents_ThenCorrectDrawOrderIsCreated()
        {
            var tree = CreateTree();

            var drawOrder = tree.GetDrawOrder();
            var drawFirst = (FakeGuiComponent)drawOrder.ElementAt(0);
            var drawSecond = (FakeGuiComponent)drawOrder.ElementAt(1);
            var drawThird = (FakeGuiComponent)drawOrder.ElementAt(2);
            var drawFourth = (FakeGuiComponent)drawOrder.ElementAt(3);
            var drawFifth = (FakeGuiComponent)drawOrder.ElementAt(4);
            var drawSixth = (FakeGuiComponent)drawOrder.ElementAt(5);

            Assert.That("A", Is.EqualTo(drawFirst.Value));
            Assert.That("B", Is.EqualTo(drawSecond.Value));
            Assert.That("C", Is.EqualTo(drawThird.Value));
            Assert.That("D", Is.EqualTo(drawFourth.Value));
            Assert.That("E", Is.EqualTo(drawFifth.Value));
            Assert.That("F", Is.EqualTo(drawSixth.Value));
        }


        [Test]
        public void WhenAddingChild_ThenCannotMakeCycleInTree()
        {
            var tree = new GuiComponentTree();
            var parent = new FakeGuiComponent("A");
            var child = new FakeGuiComponent("B");

            tree.AddComponent(parent);
            tree.AddComponent(child);
            tree.AddChild(parent, child);
  
            var parentNode = tree.FindNode(x => x.Component == parent);
            var childNode = tree.FindNode(x => x.Component == child);

            Assert.That
            (
                parentNode.ChildNodes.Contains(childNode),
                Is.True
            );

            Assert.That
            (
                parentNode.ChildNodes.Contains(parentNode),
                Is.False
            );

            Assert.That
            (
                childNode.ChildNodes.Contains(parentNode),
                Is.False
            );

            Assert.That
            (
                childNode.ChildNodes.Contains(childNode),
                Is.False
            );
        }


        [Test]
        public void WhenAddingChild_CanFindParents()
        {
            var tree = new GuiComponentTree();
            var parent = new FakeGuiComponent("A");
            var childOne = new FakeGuiComponent("B");
            var childTwo = new FakeGuiComponent("C");

            tree.AddComponent(parent);
            tree.AddComponent(childOne);
            tree.AddComponent(childTwo);
            tree.AddChild(parent, childOne);
            tree.AddChild(parent, childTwo);

            var parentNode = tree.FindNode(x => x.Component == parent);
            var childOneNode = tree.FindNode(x => x.Component == childOne);
            var childTwoNode = tree.FindNode(x => x.Component == childTwo);

            Assert.That
            (
                tree.FindParent(childOneNode) == parentNode,
                Is.True
            );
            Assert.That
            (
                tree.FindParent(childTwoNode) == parentNode,
                Is.True
            );
        }

        [Test]
        public void WhenAddingChild_ThenCannotHaveMultipleParents()
        {
            var tree = new GuiComponentTree();
            var parent = new FakeGuiComponent("A");
            var child = new FakeGuiComponent("B");
            var isolated = new FakeGuiComponent("C");

            tree.AddComponent(parent);
            tree.AddComponent(child);
            tree.AddComponent(isolated);
            tree.AddChild(parent, child);
            tree.AddChild(isolated, child);

            var parentNode = tree.FindNode(x => x.Component == parent);
            var childNode = tree.FindNode(x => x.Component == child);
            var isolatedNode = tree.FindNode(x => x.Component == isolated);

            Assert.That
            (
                isolatedNode.ChildNodes.Contains(childNode),
                Is.False
            );
            Assert.That
            (
                parentNode.ChildNodes.Contains(childNode),
                Is.True
            );
        }

        [Test]
        public void WhenDeletingChild_ThenSubChildrenDeleted()
        {
            var tree = new GuiComponentTree();
            var parent = new FakeGuiComponent("A");
            var child = new FakeGuiComponent("B");
            var subChildOne = new FakeGuiComponent("C");
            var subChildTwo = new FakeGuiComponent("D");

            tree.AddComponent(parent);
            tree.AddComponent(child);
            tree.AddComponent(subChildOne);
            tree.AddComponent(subChildTwo);
            tree.AddChild(parent, child);
            tree.AddChild(child, subChildOne);
            tree.AddChild(child, subChildTwo);

            tree.DeleteComponent(child);

            var parentNode = tree.FindNode(x => x.Component == parent);
            var childNode = tree.FindNode(x => x.Component == child);
            var subChildOneNode = tree.FindNode(x => x.Component == subChildOne);
            var subChildTwoNode = tree.FindNode(x => x.Component == subChildTwo);

            Assert.That
            (
                parentNode.ChildNodes.Contains(childNode),
                Is.False
            );

            Assert.That
            (
                childNode == null,
                Is.True
            );

            Assert.That
            (
                subChildOneNode == null,
                Is.True
            );

            Assert.That
            (
                subChildTwoNode == null,
                Is.True
            );

        }
    }

    internal class FakeGuiComponent : IGuiComponent
    {
        public string Value { get; }

        public string Text
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public bool Focus
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public GuiComponentEvents GuiComponentEvents
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public FakeGuiComponent(string value)
        {
            Value = value;
        }

        public event OnPointerClick Selected;
        public event OnPointerClick PointerClicked;
        public event OnPointerHover PointerHovering;

        public void Close() { }

        public void Initialize() { }

        public void ToggleFocus(bool value) { }

        public void Open() { }

        public void Draw()
        {
        }

        public void SetLocation(Vector2 location)
        {
        }

        public Rectangle GetClickableBounds()
        {
            return Rectangle.Empty;
        }

        public void Select()
        {
            throw new NotImplementedException();
        }

        public void TextInput(char letters)
        {
            throw new NotImplementedException();
        }

        public void PointerHover(Vector2 location)
        {
            throw new NotImplementedException();
        }

        public void PointerClick(Vector2 location)
        {
            throw new NotImplementedException();
        }

        public void Update(float elapsed)
        {
            throw new NotImplementedException();
        }

        public void PointerDown(Vector2 location)
        {
            throw new NotImplementedException();
        }

        public void Hide()
        {
            throw new NotImplementedException();
        }

        public void Show()
        {
            throw new NotImplementedException();
        }

        public Vector2 GetLocation()
        {
            throw new NotImplementedException();
        }
    }
}
