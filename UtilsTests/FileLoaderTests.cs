﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NSubstitute;
using Utils;
using TurnBasedBattle.Skills;

namespace UtilsTests
{
    [TestFixture]
    public class FileLoaderTests
    {
        [Test]
        public void WhenValidSkillObject_ThenCanSerializeSkillFile()
        {
            var fl = new FileLoader();
            var skillToSerialize = new Skill("TestSkill", 1, 1, null);

            fl.SaveJsonFor<Skill>(skillToSerialize, "TestSkill");

            Assert.That(fl.FileExists("TestSkill.json"), Is.True);
        }

        [Test]
        public void WhenValidSkillFile_ThenCanLoadSkillFile()
        {
            var fl = new FileLoader();
            var skill = fl.LoadJsonFor<Skill>("TestSkill");

            Assert.That(skill.Name == "TestSkill", Is.True);
        }
    }
}
