﻿using NSubstitute;
using NUnit.Framework;
using Graphics;

namespace TurnBasedBattle.Tests
{
    [TestFixture]
    internal class BattleMapTests
    {
        [Test]
        public void WhenBattleMapHasTiles_ThenCanGetTiles()
        {
            var tiles = new BattleMapTile[,]
            {
                {
                    new BattleMapTile(Substitute.For<IGraphicsComponent>()),
                    new BattleMapTile(Substitute.For<IGraphicsComponent>())
                },
                {
                    new BattleMapTile(Substitute.For<IGraphicsComponent>()),
                    new BattleMapTile(Substitute.For<IGraphicsComponent>())
                }
            };

            var battleMap = new BattleMap(tiles, 2, 2, 20);

            Assert.That(battleMap.GetTileAt(0, 0) == tiles[0,0], Is.True);
            Assert.That(battleMap.GetTileAt(1, 0) == tiles[1,0], Is.True);
            Assert.That(battleMap.GetTileAt(0, 1) == tiles[0,1], Is.True);
            Assert.That(battleMap.GetTileAt(1, 1) == tiles[1,1], Is.True);
            Assert.That(battleMap.GetTileAt(1, 100) == null, Is.True);
        }
    }
}
