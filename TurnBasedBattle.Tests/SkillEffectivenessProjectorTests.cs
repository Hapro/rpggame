﻿using NSubstitute;
using NUnit.Framework;
using Graphics;
using System.Collections.Generic;
using TurnBasedBattle.AI;
using TurnBasedBattle.Scripts.SkillScripts;
using System.Linq;

namespace TurnBasedBattle.Tests
{
    [TestFixture]
    class SkillEffectivenessProjectorTests
    { 
        private BattleMapActor GetTestBattleMapActor()
        {
            var ca = new CharacterData.CharacterAttributes(
                1, 1, 1, 1);
            var cs = new CharacterData.CharacterStats(ca);
            var cp = new CharacterData.CharacterProfile(
                "Test actor",
                cs,
                new List<string>()
                );
            var bcp = new BattleMapCharacterProfile(
                cp,
                new List<Skill>());

            return new BattleMapActor(bcp, Substitute.For<IGraphicsComponent>());
        }

        [Test]
        public void WhenOneSkillDoesMoreDamage_ThenCorrectSkillIsChosen()
        {
            var sep = new SkillEffectivenessProjector();
            var attackSkill = new Skill("attack", 1, null, new AttackSkillScript());
            var dudSkill1 = new DudSkill();
            var dudSkill2 = new DudSkill();
            var attacker = GetTestBattleMapActor();
            var victim = GetTestBattleMapActor();

            dudSkill1.Script.SetParameter("User", attacker);
            dudSkill2.Script.SetParameter("User", attacker);
            attackSkill.Script.SetParameter("User", attacker);

            var lst = sep.DetermineMostEffectiveSkillAgainst(new Skill[] { dudSkill1, attackSkill, dudSkill2 }, victim, sep.AttackHeuristicFunction);

            Assert.That(lst.ElementAt(0) == attackSkill, Is.True);
        }
    }

    public class DudSkill : Skill
    {
        public DudSkill()
            : base("dud", 0, null, new DudSkillScript())
        {
        }

        public DudSkill(string name, int range, StatusEffect statusEffect, SkillScript script)
            : base(name, range, statusEffect, script)
        {
        }
    }

    public class DudSkillScript : SkillScript
    {
        public DudSkillScript()
            : base("Dud")
        {
        }
    }
}
