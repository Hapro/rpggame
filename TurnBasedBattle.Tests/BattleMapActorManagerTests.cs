﻿using NSubstitute;
using NUnit.Framework;
using Graphics;
using System.Collections.Generic;

namespace TurnBasedBattle.Tests
{
    [TestFixture]
    internal class BattleMapActorManagerTests
    {
        [Test]
        public void WhenPlacingActor_ThenActorIsOnTile()
        {
            var actor = GetTestBattleMapActor();
            var battleMapActorManager = new BattleMapActorManager(actor);
            var tile = new BattleMapTile(Substitute.For<IGraphicsComponent>());

            battleMapActorManager.PutActorOnTile(actor, tile);

            Assert.That(battleMapActorManager.GetTileActorIsOn(actor) == tile, Is.True);
        }

        [Test]
        public void WhenPlacingActor_ThenActorIsNotOnOldTile()
        {
            var actor = GetTestBattleMapActor();
            var battleMapActorManager = new BattleMapActorManager(actor);
            var tile1 = new BattleMapTile(Substitute.For<IGraphicsComponent>());
            var tile2 = new BattleMapTile(Substitute.For<IGraphicsComponent>());

            battleMapActorManager.PutActorOnTile(actor, tile1);
            battleMapActorManager.PutActorOnTile(actor, tile2);

            Assert.That(battleMapActorManager.GetTileActorIsOn(actor) == tile1, Is.False);
            Assert.That(battleMapActorManager.GetTileActorIsOn(actor) == tile2, Is.True);
        }

        [Test]
        public void WhenPlacingActor_ThenFailsIfAnotherActorAlreadyOnTile()
        {
            var actor1 = GetTestBattleMapActor();
            var actor2 = GetTestBattleMapActor();
            var battleMapActorManager = new BattleMapActorManager(actor1, actor2);
            var tile = new BattleMapTile(Substitute.For<IGraphicsComponent>());

            battleMapActorManager.PutActorOnTile(actor1, tile);
            battleMapActorManager.PutActorOnTile(actor2, tile);

            Assert.That(battleMapActorManager.GetTileActorIsOn(actor1) == tile, Is.True);
            Assert.That(battleMapActorManager.GetTileActorIsOn(actor2) == tile, Is.False);
        }

        private BattleMapActor GetTestBattleMapActor()
        {
            var ca = new CharacterData.CharacterAttributes(
                1, 1, 1, 1);
            var cs = new CharacterData.CharacterStats(ca);
            var cp = new CharacterData.CharacterProfile(
                "Test actor",
                cs,
                new List<string>()
                );
            var bcp = new BattleMapCharacterProfile(
                cp,
                new List<Skill>());

            return new BattleMapActor(bcp, Substitute.For<IGraphicsComponent>());
        }
    }
}
