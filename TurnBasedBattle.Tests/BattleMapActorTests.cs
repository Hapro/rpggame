﻿using Graphics;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using TurnBasedBattle.StatusEffectScripts.Scripts;
using Utils;

namespace TurnBasedBattle.Tests
{
    [TestFixture]
    public class BattleMapActorTests
    {
        [Test]
        public void WhenAddNewStatusEffect_ThenStatusEffectAdded()
        {
            var actor = GetTestBattleMapActor();
            var statusEffect = new TestStatusEffect("Poision", 2);

            actor.AddStatusEffect(statusEffect);

            Assert.That(actor.StatusEffects.Count == 1, Is.True);
        }

        [Test]
        public void WhenAddStatusEffectWithSameName_ThenStatusEffectNotAdded()
        {
            var actor = GetTestBattleMapActor();
            var statusEffect1 = new TestStatusEffect("Poision", 1);
            var statusEffect2 = new TestStatusEffect("Poision", 1);

            actor.AddStatusEffect(statusEffect1);
            actor.AddStatusEffect(statusEffect2);

            Assert.That(actor.StatusEffects.Count == 1, Is.True);
        }

        [Test]
        public void WhenAddStatusEffectWithNegativeOrZeroDuration_ThenStatusEffectNotAdded()
        {
            var actor = GetTestBattleMapActor();
            var statusEffect1 = new TestStatusEffect("Poision", -1);
            var statusEffect2 = new TestStatusEffect("Blind", 0);

            actor.AddStatusEffect(statusEffect1);
            actor.AddStatusEffect(statusEffect2);

            Assert.That(actor.StatusEffects.Count == 0, Is.True);
        }

        [Test]
        public void WhenStatusEffectScript_ThenStatusEffectScriptDoesCorrectThing()
        {
            var script = new PoisonScript();
            var actor = GetTestBattleMapActor();

            script.SetParameter("DamagePerTurn", 5.0f);
            script.SetParameter("Actor", actor);

            script.Run();

            Assert.That(actor.BattleMapCharacterProfile.CurrentHealth == 0, Is.True);
        }

        private BattleMapActor GetTestBattleMapActor()
        {
            var ca = new CharacterData.CharacterAttributes(
                1, 1, 1, 1);
            var cs = new CharacterData.CharacterStats(ca);
            var cp = new CharacterData.CharacterProfile(
                "Test actor",
                cs,
                new List<string>()
                );
            var bcp = new BattleMapCharacterProfile(
                cp,
                new List<Skill>());

            return new BattleMapActor(bcp, Substitute.For<IGraphicsComponent>());
        }

        internal class TestStatusEffect : StatusEffect
        {
            public TestStatusEffect(string name, int duration)
                : base(name, duration)
            {
            }
        }
    }
}
