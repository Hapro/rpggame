﻿using Utils;

namespace CharacterData
{
    public class CharacterStats
    {
        public CharacterAttributes CharacterAttributes { get; }
        public DifferableInt MaxHealth { get; }
        public DifferableInt MaxMana { get; }
        public DifferableInt MovementRange { get; }

        public CharacterStats(CharacterAttributes characterAttributes)
        {
            CharacterAttributes = characterAttributes;
            MaxHealth = CharacterAttributes.Con.Value * 5;
            MaxMana = CharacterAttributes.Intel.Value * 5;
            MovementRange = CharacterAttributes.Agil.Value;
        }
    }
}
