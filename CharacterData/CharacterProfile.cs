﻿using System;
using System.Collections.Generic;

namespace CharacterData
{
    public class CharacterProfile
    {
        public string Name { get; }
        public CharacterStats CharacterStats { get; }
        public IReadOnlyCollection<string> KnownSkillNames { get; }

        public CharacterProfile(string name, CharacterStats characterStats, IReadOnlyCollection<string> knownSkillNames)
        {
            Name = name;
            CharacterStats = characterStats;
            KnownSkillNames = knownSkillNames;
        }
    }
}
