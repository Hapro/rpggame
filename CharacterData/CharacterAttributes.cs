﻿using Utils;

namespace CharacterData
{
    public class CharacterAttributes
    {
        public DifferableInt Str { get; }
        public DifferableInt Con { get; }
        public DifferableInt Intel { get; }
        public DifferableInt Agil { get; }

        public CharacterAttributes(DifferableInt str, DifferableInt con, DifferableInt intel, DifferableInt agil)
        {
            Str = str;
            Con = con;
            Intel = intel;
            Agil = agil;
        }
    }
}
