﻿using NSubstitute;
using NUnit.Framework;
using System;
using Utils;

namespace Utils.Tests
{
    [TestFixture]
    public class ScriptTests
    {
        [Test]
        public void WhenValidParameters_ThenScriptDoesWhatItIsMeantTo()
        {
            Ref<int> value = new Ref<int>(0);

            var script = new Script("TestScript",
                (x) =>
                {
                    var val = (Ref<int>)x.GetParameter("Value");
                    val.Value++;
                });

            script.RequireParameters(Tuple.Create("Value", typeof(Ref<int>)));

            script.SetParameter("Value", value);
            script.Run();

            Assert.That(value.Value, Is.EqualTo(1));
        }

        [Test]
        public void WhenRequiredParameterNotSupplied_ThenExceptionThrown()
        {
            Ref<int> value = new Ref<int>(0);
            bool ex = false;

            var script = new Script("TestScript",
                (x) =>
                {
                    var val = (Ref<int>)x.GetParameter("Value");
                    val.Value++;
                });

            script.RequireParameters(Tuple.Create("Value", typeof(int)));

            try
            {
                script.Run();
            }
            catch (Exception e)
            {
                ex = true;
            }
            Assert.That(ex, Is.True);
        }

        [Test]
        public void WhenParameterOfIncorrectType_ThenErrorThrown()
        {
            int value = 1;
            bool ex = false;

            var script = new Script("TestScript",
                (x) => { });

            script.RequireParameters(Tuple.Create("Value", typeof(float)));

            try
            {
                script.SetParameter("Value", value);
            }
            catch (Exception e)
            {
                ex = true;
            }
            Assert.That(ex, Is.True);
        }

        internal class Ref<T>
        {
            public T Value { get; set; }

            public Ref(T value) { Value = value; }
        }
    }
}
