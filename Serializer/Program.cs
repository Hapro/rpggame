﻿using System.IO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using CharacterData;
using TurnBasedBattle;
using Newtonsoft.Json.Linq;
using Utils;

namespace Serializer
{
    class Program
    {
        const string TEMPLATE_PATH = @"C:\Users\Mikiel\Documents\Organized\RPG_Game\JsonTemplates";

        static void Main(string[] args)
        {
            Console.WriteLine("Serializing...");

            //Filename : Json data
            var dict = GetDataToSerialize();
            
            Console.WriteLine("Done serializing");
            Console.WriteLine("Saving...");

            foreach(var kvp in dict)
            {
                try
                {
                    WriteJsonFile(kvp.Key, JsonConvert.SerializeObject(kvp.Value, Formatting.Indented));
                }
                catch (IOException e)
                {
                    Console.WriteLine($"Error writing {kvp.Key}. {e.Message}");
                }
            }

            Console.WriteLine("Done");
            Console.WriteLine("Testing deserialization...");

            foreach (var kvp in dict)
            {
                try
                {
                    var obj = JsonConvert.DeserializeObject(LoadJsonFile(kvp.Key));
                    Console.WriteLine(obj);
                }
                catch (IOException e)
                {
                    Console.WriteLine($"Error loading {kvp.Key}. {e.Message}");
                }
            }


            var jsonString = LoadJsonFile("Character Profile");
            var jobject = JObject.Parse(jsonString);
            var profile = jobject.ToObject(typeof(CharacterProfile));

            Console.WriteLine("Done");

            Console.ReadKey();
        }

        static string LoadJsonFile(string filename)
        {
            var sr = new StreamReader($"{TEMPLATE_PATH}\\{filename}.json");
            var str = "";

            while (!sr.EndOfStream)
            {
                str += sr.ReadToEnd();
            }
            return str;
        }

        static void WriteJsonFile(string filename, string json)
        {
            var sr = new StreamWriter($"{TEMPLATE_PATH}\\{filename}.json");
            sr.Write(json);
            sr.Close();
        }

        static Dictionary<string, object> GetDataToSerialize()
        {
            var dict = new Dictionary<string, object>();

            dict.Add("Character Attributes", new CharacterAttributes(0, 0, 0, 0));
            dict.Add("Character Stats", new CharacterStats((CharacterAttributes)dict["Character Attributes"]));
            dict.Add("Character Profile", new CharacterProfile(
                "Character Name",
                (CharacterStats)dict["Character Stats"],
                new string[]
                {
                    "Skill name"
                }));
            dict.Add("Differable Int", new DifferableInt(0));
            return dict;
        }
    }
}
