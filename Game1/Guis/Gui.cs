﻿using Graphics;
using GUI;
using GUI.Factories;
using System.Collections.Generic;
using System;

namespace Game1.Guis
{
    public abstract class Gui : IGui
    {
        protected readonly IGuiManager m_GuiManager;
        protected readonly GraphicsHandler m_GraphicsHandler;
        protected readonly IGuiComponentFactory m_GuiComponentFactory;
        protected readonly GuiGrid m_GuiGrid;
        private List<IGuiComponent> m_ComponentsAdded;

        public Gui(IGuiManager guiManager, GraphicsHandler graphicsHandler,
            IGuiComponentFactory guiComponentFactory)
        {
            m_GuiManager = guiManager;
            m_GraphicsHandler = graphicsHandler;
            m_GuiComponentFactory = guiComponentFactory;
            m_ComponentsAdded = new List<IGuiComponent>();
            m_GuiGrid = guiComponentFactory.CreateGrid();
        }

        protected void RegisterComponent(IGuiComponent component)
        {
            m_ComponentsAdded.Add(component);
            m_GuiManager.AddComponent(component);
        }

        public void Dispose()
        {
            foreach(var c in m_ComponentsAdded)
            {
                m_GuiManager.DeleteComponent(c);
            }
        }

        public void Show()
        {
            foreach (var c in m_ComponentsAdded) c.Show();
        }

        public void Hide()
        {
            foreach (var c in m_ComponentsAdded) c.Hide();
        }
    }
}
