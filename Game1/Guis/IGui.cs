﻿using System;

namespace Game1.Guis
{
    public interface IGui : IDisposable
    {
        void Show();

        void Hide();
    }
}