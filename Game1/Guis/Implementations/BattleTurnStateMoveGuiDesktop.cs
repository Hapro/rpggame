﻿using Game1.Guis.Interfaces;
using Graphics;
using GUI;
using GUI.ComponentInterface;
using GUI.Factories;
using Microsoft.Xna.Framework;

namespace Game1.Guis.Implementations
{
    public class BattleTurnStateMoveGuiDesktop : Gui, IBattleTurnStateMoveGui
    {
        public INumberInput MovePositionX { get; }

        public INumberInput MovePositionY { get; }

        public IButton ConfirmMove { get; }

        public BattleTurnStateMoveGuiDesktop(IGuiManager guiManager, GraphicsHandler graphicsHandler,
            IGuiComponentFactory guiComponentFactory)
            : base(guiManager, graphicsHandler, guiComponentFactory)
        {
            MovePositionX = m_GuiComponentFactory.CreateNumberInput(
                Vector2.Zero,
                m_GuiGrid.Unit,
                m_GuiGrid.Unit,
                Color.White);

            MovePositionY = m_GuiComponentFactory.CreateNumberInput(
                Vector2.Zero,
                m_GuiGrid.Unit,
                m_GuiGrid.Unit,
                Color.White);

            ConfirmMove = m_GuiComponentFactory.CreateButton(
                Vector2.Zero,
                m_GuiGrid.Unit * 3,
                m_GuiGrid.Unit * 2,
                Color.Green,
                "Confirm move");

            var MoveXLabel = m_GuiComponentFactory.CreateLabel(
                Vector2.Zero, m_GuiGrid.Unit * 3, m_GuiGrid.Unit, Color.White, "Move to X: ");

            var MoveYLabel = m_GuiComponentFactory.CreateLabel(
                Vector2.Zero, m_GuiGrid.Unit * 3, m_GuiGrid.Unit, Color.White, "Move to Y: ");

            MoveYLabel.Text = "Move to Y: ";
            MoveXLabel.Text = "Move to X: ";

            m_GuiGrid.AddComponent(MovePositionX, 0, 1);
            m_GuiGrid.AddComponent(MovePositionY, 0, 3);
            m_GuiGrid.AddComponent(ConfirmMove, 0, 4);
            m_GuiGrid.AddComponent(MoveXLabel, 0, 0);
            m_GuiGrid.AddComponent(MoveYLabel, 0, 2);

            m_GuiGrid.Location = new Vector2(m_GuiGrid.Unit, m_GuiGrid.Unit * 1);

            m_GuiGrid.Refresh();

            RegisterComponent(MovePositionX);
            RegisterComponent(MovePositionY);
            RegisterComponent(ConfirmMove);
            RegisterComponent(MoveXLabel);
            RegisterComponent(MoveYLabel);
        }
    }
}
