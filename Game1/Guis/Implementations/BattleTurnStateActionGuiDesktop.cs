﻿using Game1.Guis.Interfaces;
using GUI.ComponentInterface;
using Graphics;
using GUI;
using GUI.Factories;
using Microsoft.Xna.Framework;

namespace Game1.Guis.Implementations
{
    public class BattleTurnStateActionGuiDesktop : Gui, IBattleTurnStateActionGui
    {
        public INumberInput ActionLocationX { get; }

        public INumberInput ActionLocationY { get; }

        public IButton Confirm { get; }

        public IScrollList ActionList { get; }

        public BattleTurnStateActionGuiDesktop(IGuiManager guiManager, GraphicsHandler graphicsHandler, 
            IGuiComponentFactory guiComponentFactory)
            : base(guiManager, graphicsHandler, guiComponentFactory)
        {
            ActionLocationX = guiComponentFactory.CreateNumberInput(
                Vector2.Zero,
                m_GuiGrid.Unit * 2,
                m_GuiGrid.Unit,
                Color.White);

            ActionLocationY = guiComponentFactory.CreateNumberInput(
                Vector2.Zero,
                m_GuiGrid.Unit * 2,
                m_GuiGrid.Unit,
                Color.White);

            Confirm = guiComponentFactory.CreateButton(
                Vector2.Zero,
                m_GuiGrid.Unit * 6,
                m_GuiGrid.Unit * 2,
                Color.Green,
                "Do it!");

            ActionList = guiComponentFactory.CreateScrollList(
                Vector2.Zero,
                m_GuiGrid.Unit * 3,
                m_GuiGrid.Unit * 3,
                Color.White);

            var LabelX = m_GuiComponentFactory.CreateLabel(
                Vector2.Zero,
                m_GuiGrid.Unit * 2,
                m_GuiGrid.Unit,
                Color.White,
                "Action X: ");

            var LabelY = m_GuiComponentFactory.CreateLabel(
                Vector2.Zero,
                m_GuiGrid.Unit * 2,
                m_GuiGrid.Unit,
                Color.White,
                "Action Y: ");

            var LabelName = m_GuiComponentFactory.CreateLabel(
                Vector2.Zero,
                m_GuiGrid.Unit * 3,
                m_GuiGrid.Unit,
                Color.White,
                "Action name: ");

            m_GuiGrid.AddComponent(ActionLocationX, 0, 1);
            m_GuiGrid.AddComponent(ActionLocationY, 0, 3);
            m_GuiGrid.AddComponent(Confirm, 0, 4);
            m_GuiGrid.AddComponent(LabelX, 0, 0);
            m_GuiGrid.AddComponent(LabelY, 0, 2);
            m_GuiGrid.AddComponent(LabelName, 3, 0);
            m_GuiGrid.AddComponent(ActionList, 3, 1);

            m_GuiGrid.Location = new Vector2(m_GuiGrid.Unit, m_GuiGrid.Unit * 1);

            m_GuiGrid.Refresh();

            RegisterComponent(ActionLocationX);
            RegisterComponent(ActionLocationY);;
            RegisterComponent(Confirm);
            RegisterComponent(LabelX);
            RegisterComponent(LabelY);
            RegisterComponent(LabelName);
            RegisterComponent(ActionList);
        }
    }
}
