﻿using Game1.Guis.Interfaces;
using GUI.ComponentInterface;
using Graphics;
using GUI;
using GUI.Factories;
using GUI.UserInput;

namespace Game1.Guis.Implementations
{
    public class BattleGameStateGuiDesktop : Gui, IBattleTurnStateGui
    {
        public ILabel ActiveCharacterInfo { get; }

        public ILabel ActiveCharacterMoveInfo { get; }

        public UserInput UserInput { get { return m_GuiManager.UserInput; } }

        public BattleGameStateGuiDesktop(IGuiManager guiManager, GraphicsHandler graphicsHandler,
            IGuiComponentFactory guiComponentFactory) 
            : base(guiManager, graphicsHandler, guiComponentFactory)
        {
       }
    }
}
