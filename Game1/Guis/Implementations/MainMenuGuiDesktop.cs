﻿using GUI;
using Game1.Interfaces.Guis;
using GUI.ComponentInterface;
using Graphics;
using Microsoft.Xna.Framework;
using GUI.Factories;
using System;

namespace Game1.Guis.Implementations
{
    public class MainMenuGuiDesktop : Gui, IMainMenuGui
    {
        public IWindow Window { get; }
        public ITextInput TextInput { get; }

        public MainMenuGuiDesktop(IGuiManager guiManager, 
            GraphicsHandler graphicsHandler, IGuiComponentFactory guiComponentFactory) 
            : base(guiManager, graphicsHandler, guiComponentFactory)
        {
            Window = guiComponentFactory.CreateWindow(
                new Vector2(200, 200),
                200,
                200,
                Color.White);

            TextInput = guiComponentFactory.CreateTextInput(
                new Vector2(0, 100),
                100,
                50,
                Color.Gray);

            guiManager.AddComponent(Window);
            guiManager.AddComponent(TextInput);
            guiManager.AddChild(Window, TextInput);
        }
    }
}
