﻿namespace Game1.Guis.Factories
{
    public interface IGuiFactory
    {
        Gui CreateGuiForGameState(GameStateType gameStateType);
    }
}
