﻿using System;
using Game1.GuiAdapaters;
using Graphics;
using GUI;
using GUI.Factories;
using Microsoft.Xna.Framework.Content;
using Game1.Guis.Interfaces;
using Game1.GameStates.BattleGameStates;
using Game1.GameStates.MainMenuStates;
using Game1.Interfaces.Guis;

namespace Game1.Guis.Factories.Implementations
{
    public abstract class GuiFactory : IGuiFactory
    {
        protected readonly IGuiManager m_GuiManager;
        protected readonly ContentManager m_ContentManager;
        protected readonly GraphicsHandler m_GraphicsHandler;
        protected readonly IGuiComponentFactory m_GuiComponentFactory;

        public GuiFactory(IGuiManager guiManager,
            ContentManager contentManager, GraphicsHandler graphicsHandler,
            IGuiComponentFactory guiComponentFactory)
        {
            m_GraphicsHandler = graphicsHandler;
            m_GuiManager = guiManager;
            m_ContentManager = contentManager;
            m_GuiComponentFactory = guiComponentFactory;
        }

        public abstract Gui CreateGuiForGameState(GameStateType gameStateType);
    }
}
