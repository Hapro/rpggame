﻿using Graphics;
using GUI;
using Microsoft.Xna.Framework.Content;
using Game1.Guis.Implementations;
using GUI.Factories;

namespace Game1.Guis.Factories.Implementations
{
    public class DesktopGuiFactory : GuiFactory
    {
        public DesktopGuiFactory(IGuiManager guiManager, ContentManager contentManager,
            GraphicsHandler graphicsHandler, IGuiComponentFactory guiComponentFactory)
            : base(guiManager, contentManager, graphicsHandler, guiComponentFactory)
        {
        }

        public override Gui CreateGuiForGameState(GameStateType gameStateType)
        {
            switch(gameStateType)
            {
                case GameStateType.MainMenuState:
                    return new MainMenuGuiDesktop(m_GuiManager, m_GraphicsHandler, m_GuiComponentFactory);

                case GameStateType.BattleTurnStateMove:
                    return new BattleTurnStateMoveGuiDesktop(m_GuiManager, m_GraphicsHandler, m_GuiComponentFactory);

                case GameStateType.BattleTurnStateAction:
                    return new BattleTurnStateActionGuiDesktop(m_GuiManager, m_GraphicsHandler, m_GuiComponentFactory);

                case GameStateType.BattleGameState:
                    return new BattleGameStateGuiDesktop(m_GuiManager, m_GraphicsHandler, m_GuiComponentFactory);
            }

            return null;
        }
    }
}
