﻿using Game1.Guis;
using GUI.ComponentInterface;

namespace Game1.Interfaces.Guis
{
    public interface IMainMenuGui : IGui
    {
        ITextInput TextInput { get; }
    }
}
