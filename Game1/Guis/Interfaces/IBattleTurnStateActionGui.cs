﻿using GUI.ComponentInterface;

namespace Game1.Guis.Interfaces
{
    public interface IBattleTurnStateActionGui : IGui
    {
        IScrollList ActionList { get; }

        INumberInput ActionLocationX { get; }

        INumberInput ActionLocationY { get; }

        IButton Confirm { get; }
    }
}
