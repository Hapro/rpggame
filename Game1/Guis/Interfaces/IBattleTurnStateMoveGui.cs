﻿using GUI.ComponentInterface;

namespace Game1.Guis.Interfaces
{
    public interface IBattleTurnStateMoveGui : IGui
    {
        INumberInput MovePositionX { get; }

        INumberInput MovePositionY { get; }        

        IButton ConfirmMove { get; }
    }
}
