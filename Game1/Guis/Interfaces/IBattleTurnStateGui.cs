﻿using GUI.ComponentInterface;
using GUI.UserInput;

namespace Game1.Guis.Interfaces
{
    public interface IBattleTurnStateGui : IGui
    {
        ILabel ActiveCharacterInfo { get; }

        ILabel ActiveCharacterMoveInfo { get; }

        UserInput UserInput { get; }

    }
}
