﻿using Graphics;
using TurnBasedBattle;
using Microsoft.Xna.Framework;
using Game1.Factories;
using TurnBasedBattle.AI;
using TurnBasedBattle.ActionImplementations;
using Game1.Loaders;
using System.Linq;
using Game1.GuiAdapaters;
using Game1.Guis.Factories;

namespace Game1.BattleGameStates
{
    public class InitializeBattleState : GameState
    {
        public InitializeBattleState(GraphicsHandler graphicsHandler, ContentLoader contentLoader) 
            : base(graphicsHandler, contentLoader)
        {
        }

        private BattleMapCharacterProfile LoadBattleMapActorProfile(string name)
        {
            var characterProfile = m_ContentLoader.LoadCharacterProfile(name);
            var skills = characterProfile.KnownSkillNames.Select(s => m_ContentLoader.LoadSkill(name)).ToList();

            return new BattleMapCharacterProfile(characterProfile, skills);
        }

        private ScaledSpriteGraphicsComponent LoadBattleMapActorSprite(string name)
        {
            return new ScaledSpriteGraphicsComponent(
                m_GraphicsHandler,
                m_ContentLoader.LoadTexture2D(name),
                Vector2.Zero,
                38,
                63);
        }

        public override IGameState Update(float elapsed)
        {
            var tbbf = new TurnBasedBattleFactory(m_ContentLoader, m_GraphicsHandler);
            var tbb = tbbf.CreateTurnBasedBattle();
            var aiController = new AiOpponentActorController(
                new AiActorEngine(tbb.OpponentParty.BattleMapActors.ElementAt(0),
                tbb,
                new AiActorStateTransformerFactory().Generate(ActorClass.Warrior),
                new WarriorActions()));

            return new BattleGameState(
                m_GraphicsHandler,
                m_ContentLoader,
                aiController,
                tbb);
        }

        public override void Draw()
        {
            
        }

        public override void ExitTransition()
        {
        }

        public override InputAdapter CreateInputAdapter(IGuiFactory guiFactory)
        {
            return new NoInputAdapter();
        }
    }
}
