﻿using Graphics;
using TurnBasedBattle;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Linq;
using Game1.Loaders;

namespace Game1.GameStates.BattleGameStates
{
    public abstract class BattleTurnState : GameState
    {
        public TurnBasedBattleData TurnBasedBattleData { get; }
        public bool Finished { get; set; }

        protected BattleTurnState(GraphicsHandler graphicsHandler, ContentLoader contentLoader, 
            TurnBasedBattleData turnBasedBattleData)
            : base(graphicsHandler, contentLoader)
        {
            TurnBasedBattleData = turnBasedBattleData;
            SetDrawLocationsOfTiles();
            SetDrawLocationsOfAllActors();
        }

        protected virtual void SetDrawLocationOfActor(BattleMapActor actor)
        {
            var tile = TurnBasedBattleData.BattleActorManager.GetTileActorIsOn(actor);
            actor.GraphicsComponent.SetLocation(tile.GraphicsComponent.GetLocation());
        }

        protected virtual void SetDrawLocationsOfAllActors()
        {
            foreach (var actor in TurnBasedBattleData.BattleActorManager.ActorsOnMap)
            {
                SetDrawLocationOfActor(actor);
            }
        }

        protected virtual void SetDrawLocationsOfTiles()
        {
            var battleMap = TurnBasedBattleData.BattleMap;

            //draw isometric
            for (var x = 0; x < battleMap.Width; x++)
            {
                for (var y = 0; y < battleMap.Height; y++)
                {
                    var tile = battleMap.GetTileAt(x, y);
                    var location = new Vector2(
                        (y * battleMap.TileSize / 2) - (x * battleMap.TileSize / 2),
                        (x * battleMap.TileSize / 4) + (y * battleMap.TileSize / 4));
                    location.Y += tile.Elevation * (battleMap.TileSize / 2); 
                    tile.GraphicsComponent.SetLocation(location);
                }
            }
        }

        public override void Draw()
        {
            //Start a new spritebatch. Probably not the best way of doing this
            m_GraphicsHandler.SpriteBatch.End();
            m_GraphicsHandler.SpriteBatch.Begin(
                SpriteSortMode.Immediate,
                BlendState.AlphaBlend,
                SamplerState.LinearClamp,
                DepthStencilState.Default,
                RasterizerState.CullCounterClockwise,
                null,
                TurnBasedBattleData.BattleMap.Camera.GetTransform());

            TurnBasedBattleData.BattleMap.MapTiles(x =>
            {
                x.GraphicsComponent.Draw();
            });

            foreach (var a in TurnBasedBattleData.BattleActorManager.
                ActorsOnMap.OrderBy(z => z.GraphicsComponent.GetLocation().Y).ToArray())
                a.GraphicsComponent.Draw();

            m_GraphicsHandler.SpriteBatch.End();
            m_GraphicsHandler.SpriteBatch.Begin();
        }

        public override IGameState Update(float elapsed)
        {
            TurnBasedBattleData.BattleMap.MapTiles(x =>
            {
                x.GraphicsComponent.Update(elapsed);
            });

            TurnBasedBattleData.BattleActorManager.MapActors(x =>
            {
                x.GraphicsComponent.Update(elapsed);
            });

            SetDrawLocationsOfTiles();

            return this;
        }

        public override void ExitTransition()
        {
        }
    }
}
