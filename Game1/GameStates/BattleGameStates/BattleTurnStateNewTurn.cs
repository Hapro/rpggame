﻿using System.Linq;
using Graphics;
using TurnBasedBattle;
using Game1.Loaders;
using Game1.GuiAdapaters;
using Game1.Guis.Factories;

namespace Game1.GameStates.BattleGameStates
{
    class BattleTurnStateNewTurn : BattleTurnState
    {
        public BattleTurnStateNewTurn(GraphicsHandler graphicsHandler, ContentLoader contentLoader,
            TurnBasedBattleData turnBasedBattleData)
            : base(graphicsHandler, contentLoader, turnBasedBattleData)
        {
            if (TurnBasedBattleData.ActiveActor == TurnBasedBattleData.PlayerParty.BattleMapActors.ElementAt(0))
            {
                TurnBasedBattleData.ActiveActor = TurnBasedBattleData.OpponentParty.BattleMapActors.ElementAt(0);
            }
            else
            {
                TurnBasedBattleData.ActiveActor = TurnBasedBattleData.PlayerParty.BattleMapActors.ElementAt(0);
            }
        }

        public override IGameState Update(float elapsed)
        {
            TurnBasedBattleData.BattleActorManager.MapActors(z => z.UpdateEndOfTurn());

            return new BattleTurnStateMove(
                m_GraphicsHandler,
                m_ContentLoader,
                TurnBasedBattleData);
        }

        public override InputAdapter CreateInputAdapter(IGuiFactory guiFactory)
        {
            return new NoInputAdapter();
        }
    }
}
