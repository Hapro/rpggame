﻿using Graphics;
using TurnBasedBattle;
using Utils;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using Game1.Loaders;
using Game1.GuiAdapaters;
using Game1.Guis.Factories;

namespace Game1.GameStates.BattleGameStates
{
    public class BattleTurnStateMove : BattleTurnState, ITurnMove
    {
        private KeyframeAnimation m_CharacterMovingKeyframeAnimation;
        private bool MoveAnim;

        public BattleTurnStateMove(GraphicsHandler graphicsHandler,
            ContentLoader contentLoader, TurnBasedBattleData turnBasedBattleData) :
            base(graphicsHandler, contentLoader, turnBasedBattleData)
        {
            MoveAnim = false;
        }

        public bool MoveActor(int x, int y)
        {
            var tile = TurnBasedBattleData.BattleMap.GetTileAt(x, y);

            //Find path and do animation
            if (tile != null)
            {
                var pathFinder = new PathFinder();
                var actorTile = TurnBasedBattleData.BattleActorManager.GetTileActorIsOn(TurnBasedBattleData.ActiveActor);
                var path = pathFinder.GetPathTo(actorTile, tile, TurnBasedBattleData.BattleMap.GetAllTiles());
                var points = new List<Vector2>(path.Count);

                foreach (var p in path)
                {
                    var bmt = (BattleMapTile)p;
                    points.Add(bmt.GraphicsComponent.GetLocation());
                }

                m_CharacterMovingKeyframeAnimation = new KeyframeAnimation(
                    TurnBasedBattleData.ActiveActor.GraphicsComponent, points.ToArray());
                m_CharacterMovingKeyframeAnimation.FrameRate = 0.5f;

                m_CharacterMovingKeyframeAnimation.FinishedCallback = () =>
                {
                    TurnBasedBattleData.BattleActorManager.PutActorOnTile(
                        TurnBasedBattleData.ActiveActor, tile);
                    SetDrawLocationOfActor(TurnBasedBattleData.ActiveActor);
                    Finished = true;
                };

                MoveAnim = true;
            }

            return tile != null;
        }

        public override IGameState Update(float elapsed)
        {
            if (MoveAnim) m_CharacterMovingKeyframeAnimation.Update(elapsed);

            if (Finished)
            {
                return new BattleTurnStateAction(
                    m_GraphicsHandler,
                    m_ContentLoader,
                    TurnBasedBattleData);
            }

            return base.Update(elapsed);
        }

        public override InputAdapter CreateInputAdapter(IGuiFactory guiFactory)
        {
            return new BattleTurnStateMoveInputAdapter(guiFactory.CreateGuiForGameState(GameStateType.BattleTurnStateMove), this);
        }
    }
}
