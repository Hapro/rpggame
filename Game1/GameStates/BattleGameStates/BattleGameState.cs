﻿using Graphics;
using TurnBasedBattle;
using Microsoft.Xna.Framework;
using Game1.GameStates.BattleGameStates;
using Game1.Loaders;
using System.Collections.Generic;
using System.Linq;
using Game1.GuiAdapaters;
using Game1.Guis.Factories;

namespace Game1
{
    /// <summary>
    /// Base class for each 'turn' of a battle.
    /// I.e. Move character, Attack etc
    /// </summary>
    public class BattleGameState : GameState
    {
        public TurnBasedBattleData TurnBasedBattleData { get; }
        private readonly AiOpponentActorController m_OpponentActorController;
        private readonly Dictionary<BattleMapActor, ActorController> m_ActorControllerDictionary;
        protected ActorController ActiveActorController => m_ActorControllerDictionary[TurnBasedBattleData.ActiveActor];

        public BattleGameState(GraphicsHandler graphicsHandler, ContentLoader contentLoader,
            AiOpponentActorController opponentActorController, TurnBasedBattleData turnBasedBattleData)
            : base(graphicsHandler, contentLoader)
        {
            TurnBasedBattleData = turnBasedBattleData;
            m_OpponentActorController = opponentActorController;

            m_ActorControllerDictionary = new Dictionary<BattleMapActor, ActorController>
            {
                { turnBasedBattleData.PlayerParty.BattleMapActors.ElementAt(0), new GuiActorController() },
                { turnBasedBattleData.OpponentParty.BattleMapActors.ElementAt(0), opponentActorController }
            };
        }

        public void MoveCamera(int x, int y)
        {
            TurnBasedBattleData.BattleMap.Camera.Location += new Vector2(
                TurnBasedBattleData.BattleMap.TileSize * x,
                TurnBasedBattleData.BattleMap.TileSize * y);
        }

        public override IGameState Update(float elapsed)
        {
            return this;
        }

        public override void Draw()
        {
        }

        public override InputAdapter CreateInputAdapter(IGuiFactory guiFactory)
        {
            return new NoInputAdapter();
        }

        public override void ExitTransition()
        {
        }
    }
}
