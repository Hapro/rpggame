﻿using Graphics;
using TurnBasedBattle;
using System.Linq;
using Game1.Loaders;
using System;
using Game1.GuiAdapaters;
using Game1.Guis.Factories;

namespace Game1.GameStates.BattleGameStates
{
    public class BattleTurnStateAction : BattleTurnState, ITurnAction
    {
        public BattleTurnStateAction(GraphicsHandler graphicsHandler,
            ContentLoader contnetLoader, TurnBasedBattleData turnBasedBattleData)
            : base(graphicsHandler, contnetLoader, turnBasedBattleData)
        {
        }

        public bool DoAction(string actionName, int x, int y)
        {
            UseSkill(TurnBasedBattleData.ActiveActor.BattleMapCharacterProfile.KnownSkills.First(a => a.Name == actionName), x, y);
            return true;
        }

        public void UseSkill(Skill skill, int x, int y)
        {
            var tile = TurnBasedBattleData.BattleMap.GetTileAt(x, y);

            if (tile != null)
            {
                //Find the actor on this tile
                TurnBasedBattleData.BattleActorManager.MapActors(a =>
                {
                    if (TurnBasedBattleData.BattleActorManager.GetTileActorIsOn(a) == tile)
                    {
                        //Apply skill to actor
                        skill.AffectActor(a);
                    }
                });
            }

            //Go to next turn
            Finished = true;
        }

        public override IGameState Update(float elapsed)
        {
            if (Finished)
            {
                return new BattleTurnStateNewTurn(
                    m_GraphicsHandler,
                    m_ContentLoader,
                    TurnBasedBattleData);
            }

            return base.Update(elapsed);
        }

        public override InputAdapter CreateInputAdapter(IGuiFactory guiFactory)
        {
            return new BattleTurnStateActionInputAdapter(
                guiFactory.CreateGuiForGameState(GameStateType.BattleTurnStateAction),
                this,
                true);
        }
    }
}
