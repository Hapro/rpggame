﻿namespace Game1.GameStates
{
    public class GameStateUpdater
    {
        public IGameState CurrentGameState { get; private set; }

        public GameStateUpdater(IGameState startState)
        {
            CurrentGameState = startState;
        }

        public void Update(float elapsed)
        {
            var newGameState = CurrentGameState.Update(elapsed);

            if (newGameState == CurrentGameState) return;

            CurrentGameState.ExitTransition();
            CurrentGameState = newGameState;
        }

        public void Draw()
        {
            CurrentGameState.Draw();
        }
    }
}
