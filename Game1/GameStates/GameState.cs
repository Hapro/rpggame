﻿using Game1.Loaders;
using Graphics;
using Game1.GuiAdapaters;
using Game1.Guis.Factories;

namespace Game1
{
    public abstract class GameState : IGameState
    {
        protected readonly GraphicsHandler m_GraphicsHandler;
        protected readonly ContentLoader m_ContentLoader;
        public InputAdapter InputAdatper { get; }

        protected GameState(GraphicsHandler graphicsHandler, ContentLoader contentLoader)
        {
            m_GraphicsHandler = graphicsHandler;
            m_ContentLoader = contentLoader;
            InputAdatper = null;
        }

        public abstract IGameState Update(float elapsed);

        public abstract void Draw();

        public abstract InputAdapter CreateInputAdapter(IGuiFactory guiFactory);

        public virtual void ExitTransition()
        {
            InputAdatper.Dispose();
        }
    }
}
