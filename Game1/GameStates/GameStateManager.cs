﻿using Game1.Guis.Factories;
using System.Collections.Generic;
using Game1.GuiAdapaters;

namespace Game1.GameStates
{
    /// <summary>
    /// Handles changing of states and guis
    /// </summary>
    public class GameStateManager
    {
        private readonly List<GameStateUpdater> m_GameStates;

        public GameStateManager(IGameState startState)
        {
            m_GameStates = new List<GameStateUpdater> {new GameStateUpdater(startState)};
        }

        public void Update(float elapsed)
        {
            foreach (var s in m_GameStates)
            {
                //update the state. If the sub state changed, we check if
                //we need to make a new substate, and tell the parent state.
                s.Update(elapsed);
            }
        }

        public void Draw()
        {
            foreach (var s in m_GameStates) s.Draw();
        }
    }
}
