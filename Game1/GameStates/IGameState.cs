﻿using Game1.GuiAdapaters;
using Game1.Guis.Factories;
using System;
using Game1.Guis;

namespace Game1
{
    public enum GameStateType
    {
        BattleGameState,
        BattleTurnState,
        BattleTurnStateAction,
        BattleTurnStateMove,
        BattleTurnStateNewTurn,
        InitializeBattleState,
        MainMenuState
    }

    public interface IGameState
    {
        IGameState Update(float elapsed);

        void ExitTransition();

        void Draw();

        InputAdapter CreateInputAdapter(IGuiFactory guiFactory);
    }
}
