﻿using Graphics;
using Game1.Loaders;
using Game1.GuiAdapaters;
using Game1.Guis.Factories;

namespace Game1.GameStates.MainMenuStates
{
    public class MainMenuState : GameState
    {
        public MainMenuState(GraphicsHandler graphicsHandler, ContentLoader contentLoader)
            : base(graphicsHandler, contentLoader)
        {
        }

        public override IGameState Update(float elapsed)
        {
            return this;
        }

        public override void Draw()
        {
        }

        public override InputAdapter CreateInputAdapter(IGuiFactory guiFactory)
        {
            return new MainMenuInputAdapter(guiFactory.CreateGuiForGameState(GameStateType.MainMenuState), this);
        }

        public override void ExitTransition()
        {
        }
    }
}
