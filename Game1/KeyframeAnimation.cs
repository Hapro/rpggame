﻿using Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Game1
{
    /// <summary>
    /// Class used to move a graphics component
    /// </summary>
    public class KeyframeAnimation
    {
        private IReadOnlyCollection<Keyframe> m_Keyframes;
        private IGraphicsComponent GraphicsComponent { get; }
        private readonly Timer m_Timer;
        public float FrameRate { get { return m_Timer.Interval; } set { m_Timer.Interval = value; } }
        public bool Finished { get; set; }
        public bool Loop { get; set; }
        protected Keyframe CurrentKeyframe { get { return m_Keyframes.ElementAt(m_KeyFrameIndex); } }
        private int m_KeyFrameIndex;
        public Action FinishedCallback { get; set; }

        public KeyframeAnimation(IGraphicsComponent graphicsComponent, params Vector2[] points)
        {
            var frames = new List<Keyframe>();

            for (var i = 0; i < points.Length-1; i++)
            {
                frames.Add(new Keyframe(points[i], points[i + 1]));
            }

            frames.Add(new Keyframe(points[points.Length - 1], points[points.Length - 1]));

            m_Keyframes = frames;
            m_Timer = new Timer(1.0f);
            m_Timer.OnTick += NextFrame;
            Finished = false;
            Loop = false;
            m_KeyFrameIndex = 0;
            GraphicsComponent = graphicsComponent;
        }

        public virtual void Update(float elapsed)
        {
            var sx = CurrentKeyframe.StartLocation.X;
            var sy = CurrentKeyframe.StartLocation.Y;
            var ex = CurrentKeyframe.EndLocation.X;
            var ey = CurrentKeyframe.EndLocation.Y;

            var timeProportion = m_Timer.Count / m_Timer.Interval;
            var nextX = MathHelper.Lerp(sx, ex, timeProportion);
            var nextY = MathHelper.Lerp(sy, ey, timeProportion);

            if(!Finished) GraphicsComponent.SetLocation(new Vector2(nextX, nextY));

            m_Timer.Update(elapsed);
        }


        protected virtual void NextFrame()
        {
            if(m_KeyFrameIndex < m_Keyframes.Count - 1)
            {
                m_KeyFrameIndex++;
            }
            else
            {
                Finished = true;
                m_Timer.Running = false;
                FinishedCallback?.Invoke();
            }
        }

        protected class Keyframe
        {
            public Vector2 StartLocation { get; }
            public Vector2 EndLocation { get; }

            public Keyframe(Vector2 startLocation, Vector2 endLocation)
            {
                StartLocation = startLocation;
                EndLocation = endLocation;
            }
        }
    }

    public delegate void OnTimerTick();

    public class Timer
    {
        public bool Ticked { get; private set; }
        public bool Running { get; set; }
        public float Interval { get; set; }
        public float Count { get; private set; }

        public event OnTimerTick OnTick;

        public Timer(float interval)
        {
            Interval = interval;
            Count = 0;
            Running = true;
            Ticked = false;
        }

        public void Update(float elapsed)
        {
            Ticked = false;
            Count += elapsed;

            if(Count > Interval)
            {
                Ticked = true;
                Count = 0;
                OnTick?.Invoke();
            }
        }

        public void Reset()
        {
            Count = 0;
        }
    }
}
