﻿using Game1.Loaders;
using Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using TurnBasedBattle;

namespace Game1.Factories
{
    /// <summary>
    /// Creates all data necessary to begin a turn-based battle
    /// </summary>
    public class TurnBasedBattleFactory
    {
        private readonly ContentLoader m_ContentLoader;
        private readonly GraphicsHandler m_GraphicsHandler;

        public TurnBasedBattleFactory(ContentLoader contentLoader, GraphicsHandler graphicsHandler)
        {
            m_ContentLoader = contentLoader;
            m_GraphicsHandler = graphicsHandler;
        }

        private BattleMapCharacterProfile LoadBattleMapActorProfile(string name)
        {
            var characterProfile = m_ContentLoader.LoadCharacterProfile(name);
            var skills = new List<Skill>();

            foreach (var s in characterProfile.KnownSkillNames)
                skills.Add(m_ContentLoader.LoadSkill(s));

            return new BattleMapCharacterProfile(characterProfile, skills);
        }

        private ScaledSpriteGraphicsComponent LoadBattleMapActorSprite(string name)
        {
            return new ScaledSpriteGraphicsComponent(
                m_GraphicsHandler,
                m_ContentLoader.LoadTexture2D(name),
                Vector2.Zero,
                38,
                63);
        }

        private BattleMapActor[] CreateBattleMapActors()
        {
            var battleMapActors = new BattleMapActor[]
            {
                new BattleMapActor(LoadBattleMapActorProfile("squire"),
                LoadBattleMapActorSprite("squire_texture")),
                new BattleMapActor(LoadBattleMapActorProfile("dragoon"),
                LoadBattleMapActorSprite("dragoon_texture")),
            };

            return battleMapActors;
        }

        private BattleMap CreateBattleMap()
        {
            var battleMapFactory = new BattleMapFactory(
                24,
                24,
                64,
                m_GraphicsHandler,
                m_ContentLoader);

            var battleMap = battleMapFactory.Generate();

            return battleMap;
        }

        private Party CreateParty(params BattleMapActor[] actors)
        {
            return new Party(actors);
        }

        private void PlaceBattleMapActors(Party party, int x, int y,
            BattleMapActorManager battleMapActorManager, BattleMap battleMap)
        {
            var xLoc = x;

            foreach(var a in party.BattleMapActors)
            {
                BattleMapTile tile = null;

                do
                {
                    tile = battleMap.GetTileAt(xLoc, y);

                    xLoc++;

                } while (!tile.Traversable && !battleMapActorManager.IsTileEmpty(tile));

                battleMapActorManager.PutActorOnTile(a, tile);
            }
        }

        public TurnBasedBattleData CreateTurnBasedBattle()
        {
            var battleMap = CreateBattleMap();
            var battleMapActors = CreateBattleMapActors();
            var battleMapActorManager = new BattleMapActorManager(battleMapActors);
            var opponentParty = new Party(battleMapActors[0]);
            var playerParty = new Party(battleMapActors[1]);

            PlaceBattleMapActors(playerParty, 0, 0, battleMapActorManager, battleMap);
            PlaceBattleMapActors(opponentParty, 0, battleMap.Width - 2, battleMapActorManager, battleMap);

            var turnBasedBattleData = new TurnBasedBattleData(
                battleMap,
                battleMapActorManager,
                playerParty,
                opponentParty,
                false);

            return turnBasedBattleData;
        }

    }
}
