﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurnBasedBattle;
using Utils;
using Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game1.Loaders;

namespace Game1.Factories
{
    public class BattleMapFactory
    {
        private int Width { get; set; }
        private int Height { get; set; }
        private readonly int m_TileSize;
        private readonly GraphicsHandler m_GraphicsHandler;
        private readonly ContentLoader m_ContentLoader;

        public BattleMapFactory(int width, int height, int tileSize, 
            GraphicsHandler graphcisHandler, ContentLoader contentLoader)
        {
            Height = height;
            Width = width;
            m_TileSize = tileSize;
            m_GraphicsHandler = graphcisHandler;
            m_ContentLoader = contentLoader;
        }

        public BattleMap Generate()
        {
            var perlinNoise = new PerlinNoise(Width, Height, 8);
            var heightMap = perlinNoise.Generate();
            var battleMapTiles = new BattleMapTile[Width, Height];

            for(var x = 0; x < Width; x++)
            {
                for(var y = 0; y < Height; y++)
                {
                    battleMapTiles[x, y] = CreateTile(x, y, heightMap[x, y]);   
                }
            }

            return new BattleMap(battleMapTiles, Width, Height, 64);
        }

        private BattleMapTile CreateTile(int x, int y, double height)
        {
            var graphicsComponent = new ScaledSpriteGraphicsComponent(
                m_GraphicsHandler,
                m_ContentLoader.LoadTexture2D("grassBlock"),
                Vector2.Zero,
                64,
                64);
            var elevation = (int)(height * 10) / 3;
            graphicsComponent.SetColor(Color.Lerp(Color.White, Color.LightGray, (float)height));

            return new BattleMapTile(
                graphicsComponent,
                true,
                x,
                y,
                elevation);
        }
    }
}
