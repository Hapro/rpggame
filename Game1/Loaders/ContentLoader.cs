﻿using CharacterData;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using TurnBasedBattle;
using Utils;

namespace Game1.Loaders
{
    public class ContentLoader
    {
        private readonly ContentManager m_ContentManager;
        private const string TEXTURE_2D_DIRECTORY = "Textures";
        private const string CHARACTER_PROFILES_DIRECTORY = "CharacterProfiles";
        private const string SKILLS_DIRECTORY = "Skills";
        private const string STATUS_EFFECTS_DIRECTORY = "StatusEffects";

        public ContentLoader(ContentManager contentManager)
        {
            m_ContentManager = contentManager;
        }

        public Texture2D LoadTexture2D(string filename)
        {
            return m_ContentManager.Load<Texture2D>($"{ TEXTURE_2D_DIRECTORY }\\{filename}");
        }

        public CharacterProfile LoadCharacterProfile(string filename)
        {
            return m_ContentManager.Load<JsonData>($"{ CHARACTER_PROFILES_DIRECTORY }\\{filename}").Deserialize<CharacterProfile>();
        }

        public Skill LoadSkill(string filename)
        {
            return m_ContentManager.Load<JsonData>($"{ SKILLS_DIRECTORY }\\{filename}").Deserialize<Skill>();
        }

        public StatusEffect LoadStatusEffect(string filename)
        {
            return m_ContentManager.Load<JsonData>($"{ STATUS_EFFECTS_DIRECTORY }\\{filename}").Deserialize<StatusEffect>();
        }
    }
}
