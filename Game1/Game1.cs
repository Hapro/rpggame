﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using GUI;
using Graphics;
using GUI.UserInput;
using Game1.Guis.Factories.Implementations;
using GUI.Factories;
using Game1.BattleGameStates;
using Game1.GameStates;
using Utils;
using Game1.Loaders;

namespace Game1
{
    public class Game1 : Game
    {
        GraphicsHandler m_GraphicsHandler;
        GraphicsDeviceManager m_GraphicsDeviceManager;
        GuiManager m_GuiManager;
        GameStateManager m_GameStateManager;
        ContentLoader m_ContentLoader;

        public Game1()
        {
            m_GraphicsDeviceManager = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            m_ContentLoader = new ContentLoader(Content); 
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            var sb = new SpriteBatch(GraphicsDevice);
            var userInput = new UserInput();

            m_GraphicsHandler = new GraphicsHandler(m_GraphicsDeviceManager, sb);
            m_GuiManager = new GuiManager(userInput);

            m_GraphicsHandler.GraphicsDeviceManager.PreferredBackBufferWidth =
                (int)(m_GraphicsHandler.GraphicsDevice.Adapter.CurrentDisplayMode.Width * 0.8f);
            m_GraphicsHandler.GraphicsDeviceManager.PreferredBackBufferHeight =
                (int)(m_GraphicsHandler.GraphicsDevice.Adapter.CurrentDisplayMode.Height * 0.8f);

            var guiFactory = new DesktopGuiFactory(
                m_GuiManager,
                Content,
                m_GraphicsHandler,
                new GuiComponentFactoryDesktop(
                    m_GraphicsHandler,
                    Content,
                    m_GuiManager));

            var gameState = new InitializeBattleState(
                m_GraphicsHandler,
                m_ContentLoader);

            gameState.CreateInputAdapter(guiFactory);
            m_GameStateManager = new GameStateManager(gameState);

            this.IsMouseVisible = true;
        }


        protected override void UnloadContent()
        {

        }


        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            m_GameStateManager.Update((float)gameTime.ElapsedGameTime.TotalSeconds);

            m_GuiManager.Update((float)gameTime.ElapsedGameTime.TotalSeconds);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            m_GraphicsHandler.SpriteBatch.Begin();

            m_GameStateManager.Draw();
            m_GuiManager.Draw();

            m_GraphicsHandler.SpriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
