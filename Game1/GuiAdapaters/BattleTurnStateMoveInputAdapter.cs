﻿using Game1.GameStates.BattleGameStates;
using Game1.Guis;
using Game1.Guis.Interfaces;

namespace Game1.GuiAdapaters
{
    public class BattleTurnStateMoveInputAdapter: InputAdapter
    {
        private IBattleTurnStateMoveGui BattleTurnStateMoveGui => (IBattleTurnStateMoveGui)Gui;
        private readonly BattleTurnStateMove m_BattleTurnStateMove;

        public BattleTurnStateMoveInputAdapter(IGui gui, BattleTurnStateMove battleTurnStateMove)
            :base(gui)
        {
            m_BattleTurnStateMove = battleTurnStateMove;

            BattleTurnStateMoveGui.ConfirmMove.GuiComponentEvents.PointerClicked += MoveCharacter;
        }

        public void MoveCharacter()
        {
            var x = BattleTurnStateMoveGui.MovePositionX.NumberValue;
            var y = BattleTurnStateMoveGui.MovePositionY.NumberValue;

            m_BattleTurnStateMove.MoveActor(x, y);
        }

        public override void Dispose()
        {
            BattleTurnStateMoveGui.ConfirmMove.GuiComponentEvents.PointerClicked -= MoveCharacter;
            BattleTurnStateMoveGui.Dispose();
        }
    }
}
