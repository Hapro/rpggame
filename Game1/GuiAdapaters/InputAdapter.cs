﻿using Game1.Guis;
using System;

namespace Game1.GuiAdapaters
{
    /// <summary>
    /// Allows a Gui to interact with a GameState
    /// </summary>
    public abstract class InputAdapter : IDisposable
    { 
        public IGui Gui { get; private set; }

        protected InputAdapter(IGui gui)
        {
            Gui = gui;
        }

        public abstract void Dispose();
    }
}
