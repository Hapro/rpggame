﻿using Game1.GameStates.BattleGameStates;
using Game1.Guis;
using Game1.Guis.Interfaces;

namespace Game1.GuiAdapaters
{
    public class BattleTurnStateActionInputAdapter : InputAdapter
    {
        private IBattleTurnStateActionGui BattleTurnStateActionGui => (IBattleTurnStateActionGui)Gui;

        private readonly BattleTurnStateAction m_BattleTurnStateAction;
        private readonly bool m_PlayerControl;

        public BattleTurnStateActionInputAdapter(IGui gui, BattleTurnStateAction battleTurnStateAction,
            bool playerControl):base(gui)
        {
            m_BattleTurnStateAction = battleTurnStateAction;
            m_PlayerControl = playerControl;

            BattleTurnStateActionGui.Confirm.GuiComponentEvents.PointerClicked += DoAction;

            foreach (var s in m_BattleTurnStateAction.TurnBasedBattleData.ActiveActor.BattleMapCharacterProfile.KnownSkills)
            {
                BattleTurnStateActionGui.ActionList.AddItem(s.Name);
            }

            if (!m_PlayerControl) gui.Hide();
        }

        public void DoAction()
        {
            var x = BattleTurnStateActionGui.ActionLocationX.NumberValue;
            var y = BattleTurnStateActionGui.ActionLocationY.NumberValue;
            var name = BattleTurnStateActionGui.ActionList.SelectedItem;
            m_BattleTurnStateAction.DoAction(name, x, y);
        }

        public override void Dispose()
        {
            BattleTurnStateActionGui.Confirm.GuiComponentEvents.PointerClicked -= DoAction;
            BattleTurnStateActionGui.Dispose();
        }
    }
}
