﻿using Game1.Guis.Interfaces;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.Linq;
using Game1.Guis;

namespace Game1.GuiAdapaters
{
    /// <summary>
    /// All BattleGameState classes will be dispalying this GUI
    /// </summary>
    public class BattleGameStateInputAdapter : InputAdapter
    {
        private IBattleTurnStateGui BattleGameStateGui => (IBattleTurnStateGui)Gui;

        private readonly BattleGameState m_BattleGameState;

        public BattleGameStateInputAdapter(IGui gui, BattleGameState battleGameState)
            : base(gui)
        {
            m_BattleGameState = battleGameState;

            BattleGameStateGui.UserInput.KeyPressed += HandleKeyPress;
        }

        private void HandleKeyPress(IReadOnlyCollection<Keys> keys)
        {
            if (keys.Contains(Keys.Left)) m_BattleGameState.MoveCamera(1, 0);
            if (keys.Contains(Keys.Up)) m_BattleGameState.MoveCamera(0, 1);
            if (keys.Contains(Keys.Down)) m_BattleGameState.MoveCamera(0, -1);
            if (keys.Contains(Keys.Right)) m_BattleGameState.MoveCamera(-1, 0);
        }

        public override void Dispose()
        {
            BattleGameStateGui.UserInput.KeyPressed -= HandleKeyPress;
            BattleGameStateGui.Dispose();
        }

        public IBattleTurnStateGui GetGui()
        {
            return BattleGameStateGui;
        }
    }
}
