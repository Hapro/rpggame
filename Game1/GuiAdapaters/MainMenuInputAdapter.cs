﻿using Game1.GameStates.MainMenuStates;
using Game1.Guis;
using Game1.Interfaces.Guis;
using GUI.Events;

namespace Game1.GuiAdapaters
{
    public class MainMenuInputAdapter : InputAdapter
    {
        private IMainMenuGui MainMenuGui => (IMainMenuGui)Gui;
        private readonly MainMenuState m_MainMenuState;

        public MainMenuInputAdapter(IGui gui, MainMenuState mainMenuState)
            :base(gui)
        {
            m_MainMenuState = mainMenuState;
            MainMenuGui.TextInput.GuiComponentEvents.PointerClicked += ReactToEvent;
        }

        public override void Dispose()
        {
        }

        public void ReactToEvent()
        {
            MainMenuGui.TextInput.Text = "Hello World!";
            MainMenuGui.Dispose();
        }
    }
}
